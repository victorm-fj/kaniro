// @flow
import React from 'react';
import codePush from 'react-native-code-push';
import { YellowBox } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import Amplify from 'aws-amplify';
import awsmobile from './aws-exports';
import client from './client';
import Nav from './nav/Nav';
// Amplify init
Amplify.configure(awsmobile);

type Props = {};
type State = { userId: string };
class App extends React.Component<Props, State> {
	async componentWillMount() {
		client.initQueryManager();
		await client.resetStore();
	}
	componentDidMount() {
		SplashScreen.hide();
	}
	render() {
		return (
			<ApolloProvider client={client}>
				<Rehydrated>
					<Nav />
				</Rehydrated>
			</ApolloProvider>
		);
	}
}

export default codePush({
	updateDialog: true,
	installMode: codePush.InstallMode.IMMEDIATE,
})(App);
YellowBox.ignoreWarnings([
	'Warning: isMounted(...) is deprecated', // react-navigation minor bug https://github.com/react-navigation/react-navigation/issues/3956
	'Module RCTImageLoader', // react-native minor bug https://github.com/facebook/react-native/issues/17504
	'Class RCTCxxModule', // https://github.com/facebook/react-native/issues/18201
	'Module RCTBackgroundGeolocation',
	'Module RNPedometer',
	'Warning: Failed context type',
]);
