import { AsyncStorage } from 'react-native';
import uuidV4 from 'uuid/v4';

const cacheName = 'userId';
const userId = uuidV4();

export const getUserId = async () => {
	try {
		const savedUserId = await AsyncStorage.getItem(cacheName);
		if (savedUserId !== null) {
			return savedUserId;
		} else {
			await AsyncStorage.setItem(cacheName, userId);
			return userId;
		}
	} catch (error) {
		console.log('no data found -', error);
	}
};
