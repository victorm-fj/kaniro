import { createMaterialTopTabNavigator } from 'react-navigation';
import { iOSColors } from 'react-native-typography';

import Achieved from '../screens/details/achieved/Achieved';
import Goal from '../screens/details/goal/Goal';

export default createMaterialTopTabNavigator(
	{
		Achieved: { screen: Achieved },
		Goal: { screen: Goal },
	},
	{
		tabBarOptions: {
			activeTintColor: iOSColors.blue,
			inactiveTintColor: iOSColors.gray,
			indicatorStyle: { backgroundColor: iOSColors.blue },
			style: { backgroundColor: '#F7F7F7' },
			labelStyle: { fontWeight: 'bold' },
		},
	}
);
