import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { iOSColors } from 'react-native-typography';

import Notifications from '../screens/notifications/Notifications';
import Me from '../screens/me/Me';
import TrackStack from './stacks/TrackStack';
import Info from '../screens/home/Info';
import Home from '../screens/home/Home';
import CustomBottomTabBar from './components/CustomBottomTabBar';

const getIconByName = (name, focused) => {
	if (Platform.OS === 'ios') {
		return `ios-${name}${focused ? '' : '-outline'}`;
	}
	return `md-${name}`;
};

export default createBottomTabNavigator(
	{
		Home: { screen: Home },
		Dogs: { screen: Info },
		Care: { screen: Notifications },
		Events: { screen: TrackStack },
		Profile: { screen: Me },
	},
	{
		navigationOptions: ({ navigation }) => ({
			tabBarIcon: ({ focused, tintColor }) => {
				const { routeName } = navigation.state;
				let iconName;
				switch (routeName) {
				case 'Dogs':
					iconName = getIconByName('albums', focused);
					break;
				case 'Care':
					iconName = getIconByName('medical', focused);
					break;
				case 'Events':
					iconName = getIconByName('calendar', focused);
					break;
				case 'Profile':
					iconName = getIconByName('person', focused);
					break;
				default:
					iconName = getIconByName('star', focused);
					break;
				}
				return <Ionicons name={iconName} size={25} color={tintColor} />;
			},
		}),
		tabBarOptions: {
			activeTintColor: iOSColors.blue,
			inactiveTintColor: iOSColors.gray,
		},
		tabBarComponent: CustomBottomTabBar,
		tabBarPosition: 'bottom',
		animationEnabled: false,
		swipeEnabled: false,
	}
);
