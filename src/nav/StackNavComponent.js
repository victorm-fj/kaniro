import React from 'react';
import { View } from 'react-native';

import StackNav from './StackNav';
import CareActions from './components/CareActions';

class StackNavComponent extends React.Component {
	static router = StackNav.router;
	render() {
		const {
			state: { index },
		} = this.props.navigation;
		return (
			<View style={{ flex: 1 }}>
				<StackNav {...this.props} />
				{this.props.screenProps.isActive && (
					<View
						style={{
							flex: 1,
							position: 'absolute',
							top: 0,
							right: 0,
							bottom: 0,
							left: 0,
							backgroundColor: 'rgba(0,0,0,0.7)',
						}}
					/>
				)}
				{index === 0 && (
					<View
						style={{
							width: 80,
							position: 'absolute',
							bottom: 20,
							left: '50%',
							marginLeft: -40,
						}}
					>
						<CareActions isActive={this.props.screenProps.isActive} />
					</View>
				)}
			</View>
		);
	}
}

export default StackNavComponent;
