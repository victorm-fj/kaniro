import { createStackNavigator } from 'react-navigation';
import WelcomeContainer from '../../screens/intro/WelcomeContainer';
import PetInfoContainer from '../../screens/intro/pet/PetInfoContainer';
// import UserInfoContainer from '../../screens/intro/user/UserInfoContainer';

export default createStackNavigator({
	Welcome: { screen: WelcomeContainer },
	PetInfo: { screen: PetInfoContainer },
	// UserInfo: { screen: UserInfoContainer },
});
