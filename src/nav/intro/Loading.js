import React from 'react';
import { graphql } from 'react-apollo';
import { MeQuery } from '../graphQL';
import Loader from '../components/Loader';
import Error from '../components/Error';
import InitRouter from './InitRouter';
// class Loading extends React.Component {
// 	render() {
// 		return (
// 			<Query
// 				query={MeQuery}
// 				variables={{ userId: this.props.screenProps.userId }}
// 				fetchPolicy="network-only"
// 			>
// 				{({ loading, error, data }) => {
// 					if (loading) return <Loader />;
// 					if (error) return <Error error={`Error!: ${error}`} />;
// 					return <InitRouter {...this.props} user={data.me} />;
// 				}}
// 			</Query>
// 		);
// 	}
// }

const Loading = (props) => {
	const { loading, error, user } = props;
	if (loading) return <Loader />;
	if (error) return <Error error={`Error!: ${error}`} />;
	return <InitRouter {...props} user={user} />;
};

export default graphql(MeQuery, {
	options: (props) => ({
		fetchPolicy: 'network-only',
		variables: { userId: props.screenProps.userId },
	}),
	props: (props) => ({
		loading: props.data.loading,
		error: props.data.error,
		user: props.data.me,
	}),
})(Loading);
