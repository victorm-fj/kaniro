// @flow
import React from 'react';
import { createSwitchNavigator } from 'react-navigation';
import IntroNav from './intro/IntroNav';
import MainNav from './main/MainNav';
import Loading from './Loading';

const SwitchNav = createSwitchNavigator(
	{
		Loading: Loading,
		Intro: IntroNav,
		Main: MainNav,
	},
	{
		initialRouteName: 'Loading',
	}
);

type Props = {
	userId: string,
};
class AppNav extends React.Component<Props> {
	render() {
		return <SwitchNav screenProps={{ userId: this.props.userId }} />;
	}
}

export default AppNav;
