const InitRouter = ({ navigation, user }) => {
	if (user && !user.registered) {
		navigation.navigate('Main');
	} else {
		navigation.navigate('Intro');
	}
	return null;
};

export default InitRouter;
