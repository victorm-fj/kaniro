import React from 'react';
import { Query } from 'react-apollo';
import sanityClient from '@sanity/client';

import DrawerNav from './DrawerNav';
import { ActionStatusQuery } from '../graphQL';
import Loader from '../components/Loader';
import Error from '../components/Error';

const client = sanityClient({
	projectId: 'utuwdsb8',
	dataset: 'deals',
	useCdn: true,
});
class Nav extends React.Component {
	static router = DrawerNav.router;
	state = { deal: null };
	async componentDidMount() {
		try {
			// fetch the newest added deal
			const deal = await client.fetch(
				'*[_type == "deal"] | order(_createdAt desc)[0]'
			);
			console.log('fetched newest deal -', deal);
			this.setState({ deal });
		} catch (error) {
			console.log('error fetching newest deal -', error);
		}
	}
	render() {
		return (
			<Query query={ActionStatusQuery}>
				{({ loading, error, data }) => {
					if (loading) return <Loader />;
					if (error) return <Error error={`Error: ${error}`} />;
					return (
						<DrawerNav
							screenProps={{
								isActive: data.actionStatus.isActive,
								drawerLockMode: data.actionStatus.isActive
									? 'locked-closed'
									: 'unlocked',
								deal: this.state.deal,
							}}
						/>
					);
				}}
			</Query>
		);
	}
}

export default Nav;
