import React from 'react';
import { createDrawerNavigator } from 'react-navigation';
import { iOSColors } from 'react-native-typography';
import { Button, Icon } from 'react-native-elements';

import StackNavComponent from './StackNavComponent';

export default createDrawerNavigator({
	MainStack: {
		screen: StackNavComponent,
		navigationOptions: ({ navigation }) => ({
			drawerLabel: ({ focused }) => (
				<Button
					icon={
						<Icon
							name="ios-home"
							type="ionicon"
							size={22}
							color={focused ? iOSColors.white : iOSColors.blue}
						/>
					}
					title="Home"
					onPress={() => {
						navigation.toggleDrawer();
						navigation.navigate('Home');
					}}
					clear
					containerStyle={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'flex-start',
						borderColor: 'black',
						borderWidth: 2,
						paddingLeft: 15,
						backgroundColor: focused ? iOSColors.blue : iOSColors.white,
					}}
					titleStyle={{
						color: focused ? iOSColors.white : iOSColors.blue,
						fontWeight: 'bold',
					}}
				/>
			),
		}),
	},
});
