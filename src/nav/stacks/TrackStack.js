import { createStackNavigator } from 'react-navigation';
import Track from '../../screens/track/Track';
import RoutesContainer from '../../screens/track/routes/RoutesContainer';

const TrackStack = createStackNavigator({
	Track: { screen: Track, navigationOptions: { header: null } },
	Routes: { screen: RoutesContainer },
});
TrackStack.navigationOptions = ({ navigation }) => {
	return {
		tabBarVisible: navigation.state.index === 0,
	};
};

export default TrackStack;
