import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Button } from 'react-native-elements';
import Home from '../../screens/home/Home';
import PetInfoContainer from '../../screens/intro/pet/PetInfoContainer';
import ContactsContainer from '../../screens/home/contacts/ContactsContainer';

const HomeStack = createStackNavigator({
	Home: { screen: Home, navigationOptions: { header: null } },
	AddPet: { screen: PetInfoContainer },
	Contacts: {
		screen: ContactsContainer,
		navigationOptions: ({ navigation }) => ({
			title: 'Contacts',
			headerBackImage: (
				<Button
					title="Cancel"
					titleStyle={{ color: 'rgb(0,122,255)' }}
					onPress={() => navigation.goBack()}
					clear
				/>
			),
			headerStyle: { borderBottomWidth: 0 },
		}),
	},
});
HomeStack.navigationOptions = ({ navigation }) => {
	return {
		tabBarVisible: navigation.state.index === 0,
	};
};

export default HomeStack;
