import { createStackNavigator } from 'react-navigation';
import AppointmentsContainer from '../../screens/appointments/AppointmentsContainer';
import NewAppointmentContainer from '../../screens/appointments/newAppointment/NewAppointmentContainer';
import AppointmentContainer from '../../screens/appointments/appointment/AppointmentContainer';
import ChatScreenContainer from '../../screens/appointments/chat/ChatScreenContainer';

const AppointmentsStack = createStackNavigator({
	Appointments: {
		screen: AppointmentsContainer,
		navigationOptions: { header: null },
	},
	Details: { screen: AppointmentContainer },
	NewAppointment: { screen: NewAppointmentContainer },
	Chat: { screen: ChatScreenContainer },
});
AppointmentsStack.navigationOptions = ({ navigation }) => {
	return {
		tabBarVisible: navigation.state.index === 0,
	};
};

export default AppointmentsStack;
