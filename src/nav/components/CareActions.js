import React from 'react';
import { Platform, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { iOSColors } from 'react-native-typography';
import { compose, graphql } from 'react-apollo';
import { ActionButton } from '../../components/actionMenu';
import { ActionStatusQuery, UpdateActionStatus } from '../../graphQL';
import Water from '../../screens/actions/Water';
import Food from '../../screens/actions/Food';

const iconPrefix = Platform.OS === 'ios' ? 'ios' : 'md';

class CareActions extends React.Component {
	state = {
		isWater: false,
		isFood: false,
		isExercise: false,
		isAppointment: false,
	};
	onToggleAction = (action) => {
		this.onPress();
		this.setState((prevState) => ({ [action]: !prevState[action] }));
	};
	onPress = () => {
		const { isActive, updateActionStatus } = this.props;
		updateActionStatus(!isActive);
	};
	render() {
		return (
			<View>
				<ActionButton
					buttonColor={iOSColors.blue}
					size={50}
					radius={125}
					btnStartDegree={200}
					btnEndDegree={340}
					onPress={this.onPress}
					bgColor="rgba(0,0,0,0.7)"
					// icon={<Ionicons name="md-add" size={25} color="#fff" />}
				>
					<ActionButton.Item
						buttonColor={iOSColors.purple}
						title="Water"
						titleStyle={{ color: iOSColors.white }}
						onPress={() => this.onToggleAction('isWater')}
					>
						<Icon
							name={`${iconPrefix}-water`}
							color={iOSColors.white}
							type="ionicon"
						/>
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor={iOSColors.orange}
						title="Food"
						titleStyle={{ color: iOSColors.white }}
						onPress={() => this.onToggleAction('isFood')}
					>
						<Icon
							name={`${iconPrefix}-restaurant`}
							color={iOSColors.white}
							type="ionicon"
						/>
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor={iOSColors.green}
						title="Exercise"
						titleStyle={{ color: iOSColors.white }}
						onPress={() => this.onToggleAction('isExercise')}
					>
						<Icon
							name={`${iconPrefix}-football`}
							color={iOSColors.white}
							type="ionicon"
						/>
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor={iOSColors.red}
						title="Book Vet"
						titleStyle={{ color: iOSColors.white }}
						onPress={() => this.onToggleAction('isAppointment')}
					>
						<Icon
							name={`${iconPrefix}-medkit`}
							color={iOSColors.white}
							type="ionicon"
						/>
					</ActionButton.Item>
				</ActionButton>
				<Water
					isVisible={this.state.isWater}
					onSwipe={() => this.setState({ isWater: false })}
					swipeDirection="down"
				/>
				<Food
					isVisible={this.state.isFood}
					onSwipe={() => this.setState({ isFood: false })}
					swipeDirection="down"
				/>
			</View>
		);
	}
}

export default compose(
	graphql(ActionStatusQuery, {
		props: ({
			data: {
				actionStatus: { isActive },
			},
		}) => ({ isActive }),
	}),
	graphql(UpdateActionStatus, {
		props: ({ mutate }) => ({
			updateActionStatus: (isActive) => mutate({ variables: { isActive } }),
		}),
	})
)(CareActions);
