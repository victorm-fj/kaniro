import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { View, Image, StyleSheet, Platform } from 'react-native';
import { Icon } from 'react-native-elements';
import { iOSColors } from 'react-native-typography';

import TabNav from './TabNav';
import DetailNav from './DetailNav';

const iconPrefix = Platform.OS === 'ios' ? 'ios' : 'md';
const tabRoutes = ['Home', 'Dogs', 'Care', 'Events', 'Profile'];
const avatar = require('../images/Kaniro.png');

export default createStackNavigator(
	{
		TabNav: {
			screen: TabNav,
			navigationOptions: ({ navigation }) => {
				const indexTab = navigation.state.index;
				let title = tabRoutes[indexTab];
				if (indexTab === 0) {
					title = (
						<View
							style={{
								width: 70,
								height: 70,
								borderRadius: 40,
								backgroundColor: '#F7F7F7',
								justifyContent: 'center',
								alignItems: 'center',
								paddingBottom: 5,
								paddingRight: 5,
								borderWidth: StyleSheet.hairlineWidth,
								borderColor: '#A7A7AA',
							}}
						>
							<Image
								source={avatar}
								style={{ width: 60, height: 60 }}
								resizeMode="contain"
							/>
						</View>
					);
				}
				return {
					headerLeft: (
						<Icon
							name={`${iconPrefix}-menu`}
							color={iOSColors.black}
							type="ionicon"
							onPress={() => navigation.openDrawer()}
							containerStyle={{
								marginLeft: 15,
							}}
						/>
					),
					title,
					headerRight: (
						<View style={{ flexDirection: 'row' }}>
							<Icon
								name={`${iconPrefix}-cart`}
								color={iOSColors.black}
								type="ionicon"
								onPress={() => {}}
								containerStyle={{
									marginRight: 30,
								}}
							/>
							<Icon
								name={`${iconPrefix}-contact`}
								color={iOSColors.black}
								type="ionicon"
								onPress={() => {}}
								containerStyle={{
									marginRight: 15,
								}}
							/>
						</View>
					),
				};
			},
		},
		DetailNav: {
			screen: DetailNav,
			navigationOptions: ({ navigation }) => {
				return {
					title: 'Daily Detail',
					headerLeft: (
						<Icon
							name="md-close"
							color={iOSColors.black}
							type="ionicon"
							onPress={() => navigation.pop()}
							containerStyle={{
								marginLeft: 15,
							}}
						/>
					),
					headerStyle: {
						borderBottomWidth: 0,
					},
				};
			},
		},
	},
	{
		mode: 'modal',
	}
);
