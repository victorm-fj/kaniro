import React from 'react';
import { Query } from 'react-apollo';
import { MeRoutesQuery } from '../../../graphQL';
import Loader from '../../../components/Loader';
import Error from '../../../components/Error';
import Routes from './Routes';

const RoutesContainer = (props) => {
	return (
		<Query
			query={MeRoutesQuery}
			variables={{ userId: props.screenProps.userId }}
			fetchPolicy="cache-and-network"
		>
			{({ loading, error, data }) => {
				if (loading) return <Loader />;
				if (error) return <Error error={`Error: ${error}`} />;
				return <Routes {...props} data={data.me.routes} />;
			}}
		</Query>
	);
};

RoutesContainer.navigationOptions = {
	title: 'Routes',
	tabBarVisible: false,
};
export default RoutesContainer;
