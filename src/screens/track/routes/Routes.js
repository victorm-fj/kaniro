import React from 'react';
import { StyleSheet, View, FlatList, Text } from 'react-native';
import { ListItem, Button } from 'react-native-elements';
import Modal from 'react-native-modal';
import MapView, { Polyline } from 'react-native-maps';
import polyline from '@mapbox/polyline';
import moment from 'moment';

class Routes extends React.Component {
	state = { isModalVisible: false, selectedRoute: null };
	getDelta(lat, lon, distance) {
		const oneDegreeOfLatitudeInMeters = 111.32 * 1000;
		const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
		const longitudeDelta =
			distance /
			(oneDegreeOfLatitudeInMeters * Math.cos(lat * (Math.PI / 180)));
		const result = {
			latitude: lat,
			longitude: lon,
			latitudeDelta,
			longitudeDelta,
		};
		return result;
	}
	onPressRoute = (item) => {
		this.setState({ isModalVisible: true, selectedRoute: item });
	};
	onDimissModal = () => {
		this.setState({ isModalVisible: false, selectedRoute: null });
	};
	renderItem = ({ item }) => {
		return (
			<ListItem
				title={`Route ID: ${item.routeId}`}
				subtitle={'Score: 10, Distance: 1 mile'}
				chevron
				chevronColor="rgb(117,117,117)"
				onPress={() => this.onPressRoute(item)}
			/>
		);
	};
	render() {
		const { isModalVisible, selectedRoute } = this.state;
		const { data } = this.props;
		const coords = selectedRoute ? polyline.decode(selectedRoute.polyline) : [];
		return (
			<View style={styles.container}>
				<FlatList
					style={{ flex: 1 }}
					data={data.routes}
					extraData={data}
					keyExtractor={(item, index) => String(index)}
					renderItem={this.renderItem}
				/>
				<Modal
					isVisible={isModalVisible}
					onSwipe={this.onDimissModal}
					swipeDirection="down"
					hideModalContentWhileAnimating
				>
					{coords.length > 0 && (
						<MapView
							style={styles.container}
							initialRegion={this.getDelta(coords[0][0], coords[0][1], 500)}
						>
							<Polyline
								strokeColor="rgba(17, 140, 231, 0.7)"
								strokeWidth={10}
								coordinates={coords.map((coord) => ({
									latitude: coord[0],
									longitude: coord[1],
								}))}
								geodesic
							/>
						</MapView>
					)}
					{selectedRoute && (
						<View style={styles.textContainer}>
							<Text style={styles.textStyle}>
								Route ID: {selectedRoute.routeId}
							</Text>
							<Text style={styles.textStyle}>
								Start date:{' '}
								{moment(Number(selectedRoute.startDate)).format(
									'MMM D hh:mm A'
								)}
							</Text>
							<Text style={styles.textStyle}>
								End date:{' '}
								{moment(Number(selectedRoute.endDate)).format('MMM D hh:mm A')}
							</Text>
							<Text style={styles.textStyle}>Steps: {selectedRoute.steps}</Text>
							<Text style={styles.textStyle}>
								Distance: {selectedRoute.distance} m
							</Text>
						</View>
					)}
					<Button
						title="Dismis"
						onPress={this.onDimissModal}
						containerStyle={styles.buttonContainer}
					/>
				</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1 },
	textContainer: { backgroundColor: 'rgb(250, 250, 250)' },
	textStyle: { margin: 10 },
	buttonContainer: { margin: 20 },
});
export default Routes;
