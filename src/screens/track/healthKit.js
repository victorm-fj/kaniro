import React from 'react';
import { Alert } from 'react-native';
import { isAvailable, init, getSteps, getDistance } from './healthKitPromise';

export default (WrappedComponent) => {
	class HealthKit extends React.Component {
		state = { healthKit: false };
		async componentDidMount() {
			try {
				await isAvailable();
				await init();
			} catch (error) {
				console.log('HealthKit init error -', error);
				this.accessError();
			}
		}
		accessError() {
			this.setState({ healthKit: false });
			Alert.alert('Error', 'Could not access to Healthkit data', [
				{ text: 'Ok', onPress: () => {} },
			]);
		}
		async getSteps() {
			try {
				return await getSteps();
			} catch (error) {
				console.log('getSteps error -', error);
				this.accessError();
			}
		}
		async getDistance() {
			try {
				return await getDistance();
			} catch (error) {
				console.log('getDistance error -', error);
				this.accessError();
			}
		}
		render() {
			return (
				<WrappedComponent
					{...this.props}
					getSteps={this.getSteps}
					getDistance={this.getDistance}
				/>
			);
		}
	}
	return HealthKit;
};
