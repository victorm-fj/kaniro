import { graphql } from 'react-apollo';
import { SaveRouteMutation } from '../../graphQL';
import Map from './Map';

export default graphql(SaveRouteMutation, {
	options: {
		fetchPolicy: 'no-cache',
	},
	props: ({ ownProps, mutate }) => ({
		onSaveRoute: (route) =>
			mutate({
				variables: route,
				optimisticResponse: {
					__typename: 'Mutation',
					saveRoute: { __typename: 'Route', ...route, score: null },
				},
			}),
	}),
})(Map);
