import AppleHealthKit from 'rn-apple-healthkit';

const PERMS = AppleHealthKit.Constants.Permissions;
const options = {
	permissions: {
		read: [PERMS.Steps, PERMS.StepCount, PERMS.DistanceWalkingRunning],
	},
};

const promiser = (resolve, reject) => {
	return (err, data) => {
		if (err) reject(err);
		resolve(data);
	};
};

export const isAvailable = () => {
	return new Promise((resolve, reject) =>
		AppleHealthKit.isAvailable(promiser(resolve, reject))
	);
};

export const init = () => {
	return new Promise((resolve, reject) =>
		AppleHealthKit.initHealthKit(options, promiser(resolve, reject))
	);
};

export const getSteps = () => {
	return new Promise((resolve, reject) =>
		AppleHealthKit.getStepCount(null, promiser(resolve, reject))
	);
};

export const getDistance = () => {
	return new Promise((resolve, reject) =>
		AppleHealthKit.getDistanceWalkingRunning(null, promiser(resolve, reject))
	);
};
