import React from 'react';
import requirePermissions from '../../components/hocs/requirePermissions';
import Error from '../../components/Error';
import MapContainer from './MapContainer';

const permissions = ['location'];
class Track extends React.Component {
	static navigationOptions = {
		header: null,
	};
	componentDidMount() {
		const {
			permissionsStatus: { location },
			requestPermissions,
		} = this.props;
		if (location !== 'authorized') {
			requestPermissions();
		}
	}
	render() {
		const { location } = this.props.permissionsStatus;
		if (location === 'authorized') {
			return <MapContainer {...this.props} />;
		}
		return (
			<Error error="Youn need to grant permissions for this feature to work." />
		);
	}
}

export default requirePermissions(Track, permissions);
