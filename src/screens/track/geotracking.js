import React from 'react';
import { Alert, Platform } from 'react-native';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import Geolocation from 'react-native-geolocation-service';
import Loader from '../../components/Loader';

export default (WrappedComponent) => {
	class GeoTracking extends React.Component {
		state = { location: null };
		componentDidMount() {
			this.getUserCoords();
			this.setBackgroundTracking();
		}
		componentWillUnmount() {
			BackgroundGeolocation.events.forEach((event) =>
				BackgroundGeolocation.removeAllListeners(event)
			);
		}
		setBackgroundTracking() {
			BackgroundGeolocation.configure({
				desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
				stationaryRadius: 20,
				distanceFilter: Platform.OS === 'ios' ? 15 : 5,
				// notificationTitle: 'Background tracking',
				// notificationText: 'enabled',
				// debug: true,
				locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
				interval: 10000,
				fastestInterval: 5000,
				activitiesInterval: 10000,
			});
			BackgroundGeolocation.on('location', (location) => {
				console.log('location -', location);
				this.setState({ location });
			});
			BackgroundGeolocation.on('error', (error) => {
				console.log(error);
			});
			BackgroundGeolocation.on('stop', () => {
				console.log('stopped');
			});
			BackgroundGeolocation.on('start', () => {
				console.log('start');
			});
			BackgroundGeolocation.on('background', () => {
				console.log('running in background');
			});
			BackgroundGeolocation.on('foreground', () => {
				console.log('running in foreground');
			});
			BackgroundGeolocation.on('stationary', () => {
				console.log('not moving');
			});
		}
		startBackgroundTracking = () => {
			BackgroundGeolocation.start();
			this.getUserCoords();
		};
		stopBackgroundTracking = () => {
			BackgroundGeolocation.stop();
		};
		getUserCoords = () => {
			Geolocation.getCurrentPosition(
				(position) => {
					console.log('current position -', position);
					this.setState({
						location: { ...position.coords, time: position.timestamp },
					});
				},
				(error) => {
					console.log('current position error -', error);
					Alert.alert(
						'We could not get your current locations',
						error.message,
						[
							{
								text: 'Try again',
								onPress: () => this.getUserCoords(),
							},
						]
					);
				},
				{
					enableHighAccuracy: true,
					timeout: 10000,
					maximumAge: 0,
					distanceFilter: 0,
				}
			);
		};
		render() {
			if (this.state.location === null) {
				return <Loader />;
			}
			return (
				<WrappedComponent
					{...this.props}
					location={this.state.location}
					startTracking={this.startBackgroundTracking}
					stopTracking={this.stopBackgroundTracking}
				/>
			);
		}
	}
	return GeoTracking;
};
