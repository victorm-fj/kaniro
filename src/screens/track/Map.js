import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';
import MapView, { Polyline } from 'react-native-maps';
import { Button } from 'react-native-elements';
import polyline from '@mapbox/polyline';
import uuidV4 from 'uuid/v4';
import haversine from 'haversine';
import Pedometer from 'react-native-pedometer';
import geotracking from './geotracking';
import { getDelta } from './helpers';

class Map extends React.Component {
	static getDerivedStateFromProps(nextProps, prevState) {
		if (nextProps.location.accuracy <= 40) {
			let distance = 0;
			if (prevState.locations.length > 0) {
				const prevLocation =
					prevState.locations[prevState.locations.length - 1];
				const prevLatLng = {
					latitude: prevLocation.latitude,
					longitude: prevLocation.longitude,
				};
				const newLatLng = {
					latitude: nextProps.location.latitude,
					longitude: nextProps.location.longitude,
				};
				distance = haversine(prevLatLng, newLatLng, { unit: 'meter' });
			}
			return {
				locations: [...prevState.locations, nextProps.location],
				distance: prevState.distance + Math.round(distance),
			};
		}
		return null;
	}
	state = {
		tracking: false,
		locations: [],
		startDate: '',
		distance: 0,
		historicalSteps: 0,
		historicalDistance: 0,
		steps: 0,
		pedometerDistance: 0,
	};
	onStartTracking = async () => {
		const startDate = new Date();
		this.setState(
			{
				tracking: true,
				startDate: startDate.getTime().toString(),
				locations: [],
				steps: 0,
				distance: 0,
				pedometerDistance: 0,
				historicalSteps: 0,
				historicalDistance: 0,
			},
			() => {
				this.props.startTracking();
				startDate.setHours(0, 0, 0, 0);
				if (Platform.OS === 'ios') {
					Pedometer.startPedometerUpdatesFromDate(
						startDate,
						(pedometerData) => {
							console.log('pedometerData -', pedometerData);
							const { distance, numberOfSteps } = pedometerData;
							this.setState((prevState) => {
								if (prevState.historicalSteps === 0) {
									return {
										historicalSteps: numberOfSteps,
										historicalDistance: distance,
									};
								}
								return {
									steps: numberOfSteps - prevState.historicalSteps,
									pedometerDistance: Math.round(
										distance - prevState.historicalDistance
									),
								};
							});
						}
					);
				}
			}
		);
	};
	onStopTracking = async () => {
		const { startDate, steps, distance } = this.state;
		this.setState({ tracking: false });
		if (Platform.OS === 'ios') {
			Pedometer.stopPedometerUpdates();
		}
		this.props.stopTracking();
		if (distance > 0) {
			const coords = this.state.locations.map(({ latitude, longitude }) => [
				latitude,
				longitude,
			]);
			const route = {
				userId: this.props.screenProps.userId,
				routeId: uuidV4(),
				polyline: polyline.encode(coords),
				startDate,
				endDate: Date.now().toString(),
				steps: `${steps}`,
				distance: `${distance}`,
			};
			console.log('saveRoute route -', route);
			// await this.props.onSaveRoute(route);
		}
	};
	onClearPath = () => {
		this.setState({
			locations: [],
			startDate: '',
			steps: 0,
			distance: 0,
			pedometerDistance: 0,
			historicalSteps: 0,
			historicalDistance: 0,
		});
	};
	onUserLocationChange = (event) => {
		console.log('onUserLocationChange -', event.nativeEvent);
	};
	onOpenRoutes = () => {
		this.props.navigation.navigate('Routes');
	};
	getInitialRegion() {
		if (this.state.locations[0]) {
			const { latitude, longitude } = this.state.locations[0];
			return getDelta(latitude, longitude, 600);
		}
	}
	render() {
		const {
			tracking,
			locations,
			distance,
			steps,
			pedometerDistance,
		} = this.state;
		return (
			<View style={styles.container}>
				<MapView
					provider={Platform.OS === 'ios' ? null : 'google'}
					initialRegion={this.getInitialRegion()}
					ref={(map) => (this.map = map)}
					style={styles.container}
					showsUserLocation
					followsUserLocation
					onUserLocationChange={(e) => this.onUserLocationChange(e)}
				>
					<Polyline
						strokeColor="rgba(17, 140, 231, 0.7)"
						strokeWidth={10}
						coordinates={locations}
						geodesic
					/>
				</MapView>
				<View style={styles.footer}>
					<View style={styles.legends}>
						<Text style={styles.textStyle}>Distance: {distance} m</Text>
						<Text style={styles.textStyle}>Steps: {steps}</Text>
						<Text style={styles.textStyle}>
							Pedometer distance: {pedometerDistance} m
						</Text>
					</View>
					<View style={styles.buttonsContainer}>
						<Button
							title={tracking ? 'Stop tracking' : 'Start tracking'}
							onPress={tracking ? this.onStopTracking : this.onStartTracking}
							style={styles.buttonContainer}
						/>
						<Button
							title="Clear path"
							onPress={this.onClearPath}
							disabled={locations.length === 0 || tracking}
							style={styles.buttonContainer}
						/>
						{/* <Button
							title="Routes"
							onPress={this.onOpenRoutes}
							style={styles.buttonContainer}
						/> */}
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	footer: {
		position: 'absolute',
		bottom: 10,
		left: 10,
	},
	legends: {},
	buttonsContainer: {
		flexDirection: 'row',
	},
	textStyle: {
		fontSize: 14,
	},
	buttonContainer: {
		padding: 10,
	},
});
export default geotracking(Map);
