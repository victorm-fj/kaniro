import React from 'react';
import { ScrollView } from 'react-native';

import Pie from '../components/Pie';
import Stats from '../components/Stats';

const data = [
	{ id: 0, title: 'water', percentage: 20, points: 490 },
	{ id: 1, title: 'food', percentage: 30, points: 735 },
	{ id: 2, title: 'activity', percentage: 40, points: 980 },
	{ id: 3, title: 'health', percentage: 10, points: 245 },
];

const Achieved = () => {
	return (
		<ScrollView
			contentContainerStyle={{
				flexGrow: 1,
				alignItems: 'center',
				paddingBottom: 40,
			}}
		>
			<Pie data={data} />
			<Stats />
		</ScrollView>
	);
};

export default Achieved;
