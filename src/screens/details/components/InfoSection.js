import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { human, iOSColors } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

const InfoSection = (props) => {
	const { type, data } = props;
	let color,
		totalQty = 0;
	data.forEach((e) => (totalQty += e.quantity));
	switch (type) {
	case 'water':
		color = iOSColors.tealBlue;
		break;
	case 'food':
		color = iOSColors.purple;
		break;
	case 'activity':
		color = iOSColors.orange;
		break;
	case 'health':
		color = iOSColors.green;
		break;
	default:
		color = iOSColors.gray;
		break;
	}
	if (type === 'health') {
		return (
			<View>
				<View style={styles.section}>
					<View style={styles.row}>
						<Ionicons name="ios-radio-button-on" size={26} color={color} />
						<Text style={[human.headline, { paddingLeft: 10 }]}>{`${type
							.slice(0, 1)
							.toUpperCase()}${type.slice(1)}`}</Text>
					</View>
					<Text style={human.headline}>Check</Text>
				</View>
				{data.map((e) => (
					<View key={e.id} style={[styles.section, { paddingLeft: 32 }]}>
						<Text>{`${e.activity}`}</Text>
						<Ionicons
							name="ios-checkmark-circle"
							size={26}
							color={e.completed ? color : iOSColors.gray}
						/>
					</View>
				))}
			</View>
		);
	}
	return (
		<View>
			<View style={styles.section}>
				<View style={styles.row}>
					<Ionicons name="ios-radio-button-on" size={26} color={color} />
					<Text style={[human.headline, { paddingLeft: 10 }]}>{`${type
						.slice(0, 1)
						.toUpperCase()}${type.slice(1)}`}</Text>
				</View>
				<Text style={human.headline}>{`${totalQty} ${data[0].measure}`}</Text>
			</View>
			{data.map((e) => (
				<View key={e.id} style={[styles.section, { paddingLeft: 32 }]}>
					<Text>{`${e.dog}`}</Text>
					<Text>{`${e.quantity} ${e.measure}`}</Text>
				</View>
			))}
		</View>
	);
};

const styles = StyleSheet.create({
	section: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: 5,
	},
	row: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default InfoSection;
