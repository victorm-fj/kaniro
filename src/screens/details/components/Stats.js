import React from 'react';

import Card from '../../../components/card';
import InfoSection from './InfoSection';

const waterData = [
	{ id: 0, dog: 'Axo', quantity: 1.2, measure: 'L' },
	{ id: 1, dog: 'Carla', quantity: 1.1, measure: 'L' },
];
const foodData = [
	{ id: 0, dog: 'Axo', quantity: 128.0, measure: 'g' },
	{ id: 1, dog: 'Carla', quantity: 112.6, measure: 'g' },
];
const activityData = [
	{ id: 0, dog: 'Axo', quantity: 14, measure: 'min' },
	{ id: 1, dog: 'Carla', quantity: 7.8, measure: 'min' },
];
const healthData = [
	{ id: 0, activity: 'Clean Axo\'s teeth', completed: true },
	{ id: 1, activity: 'Clean Carla\'s teeth', completed: true },
	{ id: 2, activity: 'Give Axo Medication', completed: false },
];

const Stats = () => {
	return (
		<Card>
			<Card.Header title="CARE DETAIL" buttonTitle="More Care Facts" divider />
			<Card.Content>
				<InfoSection data={waterData} type="water" />
			</Card.Content>
			<Card.Content>
				<InfoSection data={foodData} type="food" />
			</Card.Content>
			<Card.Content>
				<InfoSection data={activityData} type="activity" />
			</Card.Content>
			<Card.Content>
				<InfoSection data={healthData} type="health" />
			</Card.Content>
		</Card>
	);
};

export default Stats;
