import React from 'react';
import { View } from 'react-native';

import SideInfoItem from './SideInfoItem';

const SideInfo = (props) => {
	return (
		<View style={{ justifyContent: 'space-around' }}>
			{props.data.map((e) => <SideInfoItem key={e.id} {...e} />)}
		</View>
	);
};

export default SideInfo;
