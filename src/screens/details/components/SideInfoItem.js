// @flow
import React from 'react';
import { View, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { human, iOSColors } from 'react-native-typography';

type Props = {
	id: number,
	title: string,
	percentage: number,
	points: number,
};
const SideInfoItem = (props: Props) => {
	let color;
	switch (props.title) {
	case 'water':
		color = iOSColors.tealBlue;
		break;
	case 'food':
		color = iOSColors.purple;
		break;
	case 'activity':
		color = iOSColors.orange;
		break;
	case 'health':
		color = iOSColors.green;
		break;
	default:
		color = iOSColors.gray;
		break;
	}
	return (
		<View style={{ paddingVertical: 5 }}>
			<View style={{ flexDirection: 'row' }}>
				<Ionicons name="ios-radio-button-on" size={16} color={color} />
				<Text style={[human.footnote, { fontWeight: 'bold', paddingLeft: 5 }]}>
					{`${props.title.slice(0, 1).toUpperCase()}${props.title.slice(1)}`}
				</Text>
			</View>
			<Text style={[human.footnote, { color: iOSColors.gray }]}>
				{`${props.percentage}%-${props.points} Ps`}
			</Text>
		</View>
	);
};

export default SideInfoItem;
