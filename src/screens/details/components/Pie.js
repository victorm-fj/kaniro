import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { human, iOSColors } from 'react-native-typography';
import { VictoryPie, VictoryLabel } from 'victory-native';
import Svg from 'react-native-svg';

import Card from '../../../components/card';
import SideInfo from './SideInfo';

const { width, height } = Dimensions.get('window');
const data = [
	{ x: 'Water', y: 490 },
	{ x: 'Food', y: 735 },
	{ x: 'Activity', y: 980 },
	{ x: 'Health', y: 245 },
];
const colorScale = [
	iOSColors.tealBlue,
	iOSColors.purple,
	iOSColors.orange,
	iOSColors.green,
];

const Pie = (props) => {
	return (
		<Card>
			<Card.Header style={{ justifyContent: 'center', alignItems: 'center' }}>
				<Text style={[human.body, { color: iOSColors.gray }]}>
					The best care ratio for Save money
				</Text>
			</Card.Header>
			<Card.Content
				style={{ flexDirection: 'row', justifyContent: 'space-between' }}
			>
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Svg width={width * 0.5} height={height * 0.3}>
						<VictoryPie
							data={data}
							innerRadius={75}
							colorScale={colorScale}
							width={width * 0.5}
							height={height * 0.3}
							style={{
								parent: {
									justifyContent: 'center',
									alignItems: 'center',
								},
							}}
							labels={() => null}
							padding={{ top: 0, bottom: 20, left: 5 }}
							standalone={false}
						/>
						<VictoryLabel
							textAnchor="middle"
							verticalAnchor="middle"
							x={width * 0.5 / 2 + 5}
							y={height * 0.3 / 2 - 20}
							text={'2500'}
							style={{ fontSize: 36 }}
						/>
						<VictoryLabel
							textAnchor="middle"
							verticalAnchor="middle"
							x={width * 0.5 / 2 + 5}
							y={height * 0.3 / 2 + 10}
							text={'Points'}
							style={{
								fontSize: 16,
								color: '#d3d3d3',
							}}
						/>
					</Svg>
				</View>
				<View style={{ flex: 0.5, paddingLeft: 16 }}>
					<SideInfo data={props.data} />
				</View>
			</Card.Content>
		</Card>
	);
};

export default Pie;
