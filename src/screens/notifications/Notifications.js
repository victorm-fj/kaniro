// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';
class Notifications extends React.Component<*, *> {
	render() {
		return (
			<View style={styles.container}>
				<Text h1>Empty screen</Text>
				{this.props.screenProps.isActive && (
					<View
						style={{
							flex: 1,
							position: 'absolute',
							top: 0,
							right: 0,
							bottom: 0,
							left: 0,
							backgroundColor: 'rgba(0,0,0,0.7)',
							zIndex: 1,
						}}
					/>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
export default Notifications;
