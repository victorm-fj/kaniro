// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Button } from 'react-native-elements';
import branch from 'react-native-branch';
import Modal from 'react-native-modal';

class Me extends React.Component<*, *> {
	state = { isModalVisible: false, credits: 0 };
	onOpenModal = () => {
		this.setState({ isModalVisible: true });
	};
	onDimissModal = () => {
		this.setState({ isModalVisible: false, credits: 0 });
	};
	onGetRewards = async () => {
		const { credits } = await branch.loadRewards();
		console.log('credits -', credits);
		this.setState({ credits });
	};
	onReedemCredits = async () => {
		const redeemResult = await branch.redeemRewards(
			this.state.credits,
			'default'
		);
		console.log('redeemResult -', redeemResult);
		this.onOpenModal();
	};
	render() {
		return (
			<View style={styles.container}>
				<Text h1>Me screen</Text>
				<Text style={styles.text}>
					Referral credits: {this.state.credits} points
				</Text>
				<Button
					title="Get rewards"
					onPress={this.onGetRewards}
					containerStyle={styles.buttonContainer}
				/>
				<Button
					title="Reedem credits"
					onPress={this.onReedemCredits}
					containerStyle={styles.buttonContainer}
					disabled={this.state.credits === 0}
				/>
				<Modal
					isVisible={this.state.isModalVisible}
					hideModalContentWhileAnimating
					onSwipe={this.onDimissModal}
					swipeDirection="down"
				>
					<View style={styles.modalView}>
						<Text h4 style={styles.text}>
							Congrats! You've just redeemed {this.state.credits} credits for
							your pet's food!
						</Text>
						<Text h4 style={styles.text}>
							Delivery's on the road :D
						</Text>
					</View>
					<Button
						title="Dismiss"
						onPress={this.onDimissModal}
						containerStyle={styles.buttonContainer}
					/>
				</Modal>
				{this.props.screenProps.isActive && (
					<View
						style={{
							flex: 1,
							position: 'absolute',
							top: 0,
							right: 0,
							bottom: 0,
							left: 0,
							backgroundColor: 'rgba(0,0,0,0.7)',
							zIndex: 1,
						}}
					/>
				)}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	modalView: {
		flex: 1,
		margin: 20,
		marginTop: 40,
		backgroundColor: 'rgb(250,250,250)',
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: { margin: 20 },
	buttonContainer: { margin: 20 },
});
export default Me;
