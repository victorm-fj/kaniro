import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';
// import { Query } from 'react-apollo';
// import { MeAppointmentsQuery } from '../../graphQL';
// import Loader from '../../components/Loader';
// import Error from '../../components/Error';
// import Appointments from './Appointments';

class AppointmentsContainer extends React.Component<*, *> {
	render() {
		return (
			<View style={styles.container}>
				<Text h1>Notifications screen</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
export default AppointmentsContainer;

// const AppointmentsContainer = (props) => {
// 	return (
// 		<Query
// 			query={MeAppointmentsQuery}
// 			variables={{ userId: props.screenProps.userId }}
// 			fetchPolicy="cache-and-network"
// 		>
// 			{({ loading, error, data }) => {
// 				if (loading) return <Loader />;
// 				if (error) return <Error error={`Error!: ${error}`} />;
// 				return <Appointments {...props} user={data.me} />;
// 			}}
// 		</Query>
// 	);
// };

// export default AppointmentsContainer;
