import moment from 'moment';
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import uuidV4 from 'uuid/v4';

const getDateString = (date: Date): string => {
	const isTodayNumber = moment().diff(date, 'hours');
	if (isTodayNumber === 0) {
		return moment(date).format('[Today] H:mm A');
	}
	return moment(date).format('ddd MMM D H:mm A');
};
class Appointment extends React.Component {
	onPressOpenChat = async () => {
		let {
			appointment: {
				appointmentId,
				conversation: { conversationId },
			},
		} = this.props.navigation.state.params;
		if (conversationId === null) {
			conversationId = uuidV4();
			const createdAt = Date.now().toString();
			const conversation = { appointmentId, conversationId, createdAt };
			console.log(conversation);
			await this.props.onCreateConversation(conversation);
		}
		this.props.navigation.navigate('Chat', { conversationId });
	};
	render() {
		const {
			appointment: {
				consultantType,
				date,
				description,
				mediaType,
				pet: { name },
			},
		} = this.props.navigation.state.params;
		return (
			<View style={styles.container}>
				<View style={styles.details}>
					<Text style={styles.text}>{`Pet: ${name}`}</Text>
					<Text
						style={styles.text}
					>{`Consultant type: ${consultantType}`}</Text>
					<Text style={styles.text}>{`Date: ${getDateString(
						new Date(Number(date))
					)}`}</Text>
					<Text style={styles.text}>{`Description: ${description}`}</Text>
					{mediaType === 'chat' && (
						<Button
							title="Open chat"
							onPress={this.onPressOpenChat}
							containerStyle={styles.buttonContainer}
						/>
					)}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { flex: 1 },
	details: { flex: 1, margin: 10 },
	text: { marginTop: 10 },
	chat: { flex: 2, borderWidth: 2, borderColor: 'black' },
	buttonContainer: { padding: 20 },
});
export default Appointment;
