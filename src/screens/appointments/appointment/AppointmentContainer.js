import { graphql } from 'react-apollo';
import { CreateConversationMutation } from '../../../graphQL';
import Appointment from './Appointment';

export default graphql(CreateConversationMutation, {
	options: {
		fetchPolicy: 'no-cache',
	},
	props: ({ ownProps, mutate }) => ({
		onCreateConversation: (conversation) =>
			mutate({
				variables: conversation,
				optimisticResponse: {
					__typename: 'Mutation',
					createConversation: {
						__typename: 'Conversation',
						...conversation,
					},
				},
			}),
	}),
})(Appointment);
