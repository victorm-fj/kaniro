// @flow
import React from 'react';
import { StyleSheet, View, Image, FlatList, Dimensions } from 'react-native';
import { ListItem } from 'react-native-elements';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import ImagePicker from '../../../components/imagePicker/ImagePicker';

const { width } = Dimensions.get('window');
const IMAGES_PER_ROW = 3;
const calculatedSize = () => {
	const size = width / IMAGES_PER_ROW;
	return { width: size, height: size };
};
type ImageType = { type: string, data: string };
type Props = {
	images: Array<ImageType>,
	onBlur: () => void,
	onFocus: () => void,
	onChangeText: () => void,
	scrollToEnd: () => void,
	addImage: () => void,
};
// type State = {
// 	images: Array<ImageType>,
// };
class Description extends React.Component<Props> {
	// state = { images: [] };
	// addImage = (image: ImageType) => {
	// 	this.setState((prevState) => ({ images: [...prevState.images, image] }));
	// 	this.props.scrollToEnd();
	// };
	renderItem = ({ item }: { item: ImageType }) => {
		console.log('cardItem -', item);
		return (
			<Image
				style={calculatedSize()}
				resizeMode="cover"
				source={{ uri: `data:image/${item.type};base64,${item.data}` }}
			/>
		);
	};
	render() {
		const { onBlur, onFocus, onChangeText } = this.props;
		return (
			<View>
				<ListItem
					containerStyle={{ marginTop: 20 }}
					contentContainerStyle={styles.listContainer}
					title={
						<AutoGrowingTextInput
							placeholder="Describe your pet's symptoms"
							enableScrollToCaret
							onBlur={onBlur}
							onFocus={onFocus}
							onChangeText={onChangeText}
						/>
					}
					rightIcon={<ImagePicker addImage={this.props.addImage} />}
				/>
				<FlatList
					style={{ flex: 1 }}
					data={this.props.images}
					extraData={this.props.images}
					keyExtractor={(item, index) => String(index)}
					renderItem={this.renderItem}
					horizontal
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	listContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	// imagePicker: {
	// 	position: 'absolute',
	// 	top: 0,
	// 	right: 0,
	// },
});
export default Description;
