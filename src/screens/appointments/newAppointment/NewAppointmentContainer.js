import { graphql } from 'react-apollo';
import { BookAppointmentMutation, MeAppointmentsQuery } from '../../../graphQL';
import NewAppointment from './NewAppointment';

export default graphql(BookAppointmentMutation, {
	props: ({ ownProps, mutate }) => ({
		onBookAppointment: (appointment, pet) =>
			mutate({
				variables: appointment,
				optimisticResponse: {
					__typename: 'Mutation',
					bookAppointment: {
						__typename: 'Appointment',
						...appointment,
						photos: appointment.photos.map((photo) => ({
							__typename: 'Image',
							...photo,
						})),
						pet: {
							...pet,
							image: {
								__typename: 'Image',
								region: '',
								bucket: '',
								key: '',
							},
						},
						conversation: { __typename: 'Conversation', conversationId: null },
					},
				},
			}),
	}),
	options: (props) => ({
		fetchPolicy: 'cache-and-network',
		update: (proxy, { data: { bookAppointment } }) => {
			const userId = props.screenProps.userId;
			const data = proxy.readQuery({
				query: MeAppointmentsQuery,
				variables: { userId },
			});
			data.me.appointments = data.me.appointments
				? [
					bookAppointment,
					...data.me.appointments.filter(
						(appointment) =>
							appointment.appointmentId !== bookAppointment.appointmentId
					),
				  ]
				: [bookAppointment];
			proxy.writeQuery({
				query: MeAppointmentsQuery,
				variables: { userId },
				data,
			});
		},
	}),
})(NewAppointment);
