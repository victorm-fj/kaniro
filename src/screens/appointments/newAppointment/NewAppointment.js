// @flow
import React from 'react';
import {
	StyleSheet,
	View,
	Platform,
	KeyboardAvoidingView,
	Text,
	Keyboard,
	ScrollView,
	DatePickerAndroid,
	TimePickerAndroid,
} from 'react-native';
import { Button, Icon, ListItem, ButtonGroup } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { NavigationScreenProp } from 'react-navigation';
import uuidV4 from 'uuid/v4';
import { uploadImage } from '../../../utils/s3';
import { asyncForEach } from '../../../utils/helpers';
import awsmobile from '../../../aws-exports';
import CustomPicker from '../../../components/customPicker/CustomPicker';
import CustomDatePicker from '../../../components/customPicker/CustomDatePicker';
import Loader from '../../../components/Loader';
import Description from './Description';

const icon =
	Platform.OS === 'ios'
		? 'ios-information-circle-outline'
		: 'md-information-circle';
const platformBehavior = Platform.OS === 'ios' ? 'padding' : 'height';
const consultantTypes = [
	{ label: 'GP', value: 'gp' },
	{ label: 'Specialist', value: 'specialist' },
	{ label: 'Therapist', value: 'therapist' },
];
const getDateString = (date: Date): string => {
	const isTodayNumber = moment().diff(date, 'hours');
	if (isTodayNumber === 0) {
		return moment(date).format('[Today], h:mm A');
	}
	return moment(date).format('ddd MMM D, h:mm A');
};

type ImageType = { type: string, data: string };
type Props = {
	navigation: NavigationScreenProp<*, *>,
	screenProps: NavigationScreenProp<*, *>,
};
type State = {
	images: Array<ImageType>,
	message: string,
	pickerPet: boolean,
	pickerValue: boolean,
	pickerDate: boolean,
	selectedValue: string,
	selectedIndex: number,
	selectedDate: Date,
	mediaType: string,
	descriptionY: number,
	booking: boolean,
	petId: string,
	androidYear: number,
	androidMonth: number,
	androidDay: number,
};
class NewAppointment extends React.Component<Props, State> {
	static navigationOptions = {
		title: 'New Appointment',
		headerRight: (
			<Icon
				name={icon}
				color="rgb(88,86,214)"
				type="ionicon"
				onPress={() => {}}
				containerStyle={{
					paddingRight: 15,
				}}
			/>
		),
		tabBarVisible: false,
	};
	state = {
		images: [],
		message: '',
		pickerPet: false,
		pickerValue: false,
		pickerDate: false,
		selectedValue: '',
		selectedIndex: 0,
		selectedDate: new Date(),
		mediaType: 'chat',
		descriptionY: 0,
		booking: false,
		petId: this.props.navigation.state.params.user.pets[0].petId,
		androidYear: 0,
		androidMonth: 0,
		androidDay: 0,
	};
	addImage = (image: ImageType) => {
		this.setState((prevState) => ({ images: [...prevState.images, image] }));
		this.scrollToEnd();
	};
	onFocusHandler = () => {
		if (this.description) {
			this.description.measure((x, y, width, height, pageX, pageY) => {
				console.log('this.description pageY -', pageY);
				if (this.state.descriptionY === 0) {
					this.setState({ descriptionY: pageY });
				}
				setTimeout(() => {
					if (this.scrollView) {
						this.scrollView.scrollTo({
							x: 0,
							y: this.state.descriptionY - 64,
							animated: true,
						});
					}
				}, 200);
			});
		}
	};
	scrollToEnd = () => {
		if (this.scrollView) {
			setTimeout(() => this.scrollView.scrollToEnd(), 200);
		}
	};
	onBlurHandler = () => {
		Keyboard.dismiss();
	};
	showTimePickerAndroid = async (stateValue: string) => {
		const date = new Date();
		try {
			const { action, hour, minute } = await TimePickerAndroid.open({
				hour: date.getHours(),
				minute: date.getMinutes(),
				is24Hour: false,
			});
			if (action !== TimePickerAndroid.dismissedAction) {
				const { androidYear, androidMonth, androidDay } = this.state;
				const hours = hour;
				const minutes = minute;
				this.setState({
					[stateValue]: new Date(
						androidYear,
						androidMonth,
						androidDay,
						hours,
						minutes
					),
				});
			}
		} catch (error) {
			console.log('showTimePickerAndroid error -', error);
		}
	};
	showAndroidDatePicker = async (stateValue: string) => {
		const date = new Date();
		try {
			const { action, year, month, day } = await DatePickerAndroid.open({
				date,
				mode: 'spinner',
			});
			if (action !== DatePickerAndroid.dismissedAction) {
				this.setState({
					androidYear: year,
					androidMonth: month,
					androidDay: day,
				});
				this.showTimePickerAndroid(stateValue);
			}
		} catch (error) {
			console.log('showAndroidDatePicker error -', error);
		}
	};
	onChangeTextHandler = (message: string) => {
		this.setState({ message: message.trim() });
	};
	onPressSelectPet = () => {
		this.setState({ pickerPet: true });
	};
	onPressConsultant = () => {
		this.setState({ pickerValue: true });
	};
	onPressDate = () => {
		if (Platform.OS === 'ios') {
			this.setState({ pickerDate: true });
		} else {
			this.showAndroidDatePicker('selectedDate');
		}
	};
	setSelectedPet = (selectedValue: string) => {
		this.setState({ pickerPet: false, petId: selectedValue });
	};
	setSelectedValue = (selectedValue: string, selectedIndex: number) => {
		this.setState({ pickerValue: false, selectedValue, selectedIndex });
	};
	setSelectedDate = (selectedDate: Date) => {
		this.setState({ pickerDate: false, selectedDate });
	};
	onPressMediaType = (selectedIndex: number) => {
		const mediaTypes = ['video', 'phone', 'chat'];
		this.setState({ mediaType: mediaTypes[selectedIndex] });
	};
	onPressBook = async () => {
		this.setState({ booking: true });
		const {
			selectedIndex,
			selectedDate,
			mediaType,
			message,
			petId,
			images,
		} = this.state;
		const {
			user: { userId, pets },
		} = this.props.navigation.state.params;
		const photos = [];
		if (images.length > 0) {
			await asyncForEach(images, async (image) => {
				const { type, data } = image;
				const imageBuffer = new Buffer(data, 'base64');
				const mimeType = type === 'jpg' ? 'image/jpeg' : `image/${type}`;
				const visibility = 'public';
				const key = `${visibility}/${userId}/${petId}.${type}`;
				const bucket = awsmobile.aws_user_files_s3_bucket;
				const region = awsmobile.aws_project_region;
				const photo = { bucket, key, region };
				photos.push(photo);
				await uploadImage(key, mimeType, imageBuffer);
			});
		}
		const appointment = {
			userId,
			appointmentId: uuidV4(),
			consultantType: consultantTypes[selectedIndex].value,
			date: selectedDate.getTime().toString(),
			mediaType,
			description: message,
			petId,
			specialistId: 'specialist0',
			photos,
		};
		const selectedPet = pets.filter((pet) => pet.petId === petId);
		console.log('appointment -', appointment);
		console.log('selectedPet -', selectedPet);
		await this.props.onBookAppointment(appointment, selectedPet[0]);
		this.setState({ booking: false }, () => this.props.navigation.goBack());
	};
	renderButtons() {
		const { mediaType } = this.state;
		const button1 = () => (
			<Button
				title="Video"
				icon={
					<Ionicons
						name={`ios-videocam${mediaType === 'video' ? '' : '-outline'}`}
						size={36}
						color="purple"
					/>
				}
				buttonStyle={{ backgroundColor: 'transparent' }}
				titleStyle={{ color: 'rgb(0,0,0)' }}
				onPress={() => this.setState({ mediaType: 'video' })}
			/>
		);
		const button2 = () => (
			<Button
				title="Phone"
				icon={
					<Ionicons
						name={`ios-call${mediaType === 'phone' ? '' : '-outline'}`}
						size={36}
						color="purple"
					/>
				}
				buttonStyle={{ backgroundColor: 'transparent' }}
				titleStyle={{ color: 'rgb(0,0,0)' }}
				onPress={() => this.setState({ mediaType: 'phone' })}
			/>
		);
		const button3 = () => (
			<Button
				title="Chat"
				icon={
					<Ionicons
						name={`ios-chatbubbles${mediaType === 'chat' ? '' : '-outline'}`}
						size={36}
						color="purple"
					/>
				}
				buttonStyle={{ backgroundColor: 'transparent' }}
				titleStyle={{ color: 'rgb(0,0,0)' }}
				onPress={() => this.setState({ mediaType: 'chat' })}
			/>
		);
		return [{ element: button1 }, { element: button2 }, { element: button3 }];
	}
	render() {
		const {
			pickerPet,
			pickerValue,
			pickerDate,
			selectedIndex,
			selectedDate,
			booking,
			petId,
			message,
		} = this.state;
		const {
			navigation: {
				state: {
					params: {
						user: { pets },
					},
				},
			},
		} = this.props;
		const selectedPet = pets.filter((pet) => pet.petId === petId);
		const petName = `Pet: ${selectedPet[0].name}`;
		return (
			<KeyboardAvoidingView
				style={styles.container}
				keyboardVerticalOffset={64}
				behavior={platformBehavior}
			>
				{booking && <Loader />}
				{pickerPet && (
					<CustomPicker
						options={pets.map((pet) => ({ label: pet.name, value: pet.petId }))}
						setSelectedValue={this.setSelectedPet}
					/>
				)}
				{pickerValue && (
					<CustomPicker
						options={consultantTypes}
						setSelectedValue={this.setSelectedValue}
					/>
				)}
				{pickerDate && (
					<CustomDatePicker setSelectedDate={this.setSelectedDate} />
				)}
				<ScrollView
					style={styles.container}
					ref={(scrollView) => (this.scrollView = scrollView)}
				>
					<ListItem
						roundAvatar
						leftAvatar={leftAvatarProps}
						title={petName}
						containerStyle={[styles.item, { marginTop: 20 }]}
						chevron
						chevronColor="rgb(117,117,117)"
						onPress={this.onPressSelectPet}
					/>
					<View style={{ marginTop: 20 }}>
						<ListItem
							leftIcon={{ name: 'healing', type: 'ionicons' }}
							title="Consultant type"
							subtitle={consultantTypes[selectedIndex].label}
							containerStyle={styles.item}
							contentContainerStyle={styles.selectorContainer}
							bottomDivider
							chevron
							chevronColor="rgb(117,117,117)"
							subtitleStyle={{ color: 'rgb(117,117,117)' }}
							onPress={this.onPressConsultant}
						/>
						<ListItem
							leftIcon={{ name: 'access-time', type: 'ionicons' }}
							title="Date & time"
							subtitle={getDateString(selectedDate)}
							containerStyle={styles.item}
							contentContainerStyle={styles.selectorContainer}
							bottomDivider
							chevron
							chevronColor="rgb(117,117,117)"
							subtitleStyle={{ color: 'rgb(117,117,117)' }}
							onPress={this.onPressDate}
						/>
					</View>
					<ButtonGroup
						buttons={this.renderButtons()}
						containerStyle={styles.buttonGroupContainer}
						onPress={this.onPressMediaType}
						buttonStyle={{ backgroundColor: 'rgb(250,250,250)' }}
					/>
					<View ref={(description) => (this.description = description)}>
						<Description
							onBlur={this.onBlurHandler}
							onFocus={this.onFocusHandler}
							onChangeText={this.onChangeTextHandler}
							scrollToEnd={this.scrollToEnd}
							addImage={this.addImage}
							images={this.state.images}
						/>
					</View>
					<Text style={{ marginTop: 20, marginHorizontal: 10 }}>
						Attach notes and photos to help provide you with the best advice.
					</Text>
					<Button
						title="Book"
						containerStyle={styles.bookButtonContainer}
						buttonStyle={{
							backgroundColor:
								message === '' ? 'rgba(88,86,214,0.5)' : 'rgb(88,86,214)',
						}}
						onPress={this.onPressBook}
						disabled={message === ''}
					/>
				</ScrollView>
			</KeyboardAvoidingView>
		);
	}
}

const leftAvatarProps = {
	source: {
		uri:
			'https://vidzemesslimnica.lv/wp-content/uploads/2016/05/Unknown_v%C4%ABrietis.png',
	},
	rounded: true,
	width: 50,
	height: 50,
};
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	item: {
		backgroundColor: '#fafafa',
	},
	selectorContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	buttonGroupContainer: {
		width: '100%',
		height: 60,
		marginTop: 20,
		marginLeft: 0,
		marginRight: 0,
		backgroundColor: 'rgb(250,250,250)',
	},
	bookButtonContainer: {
		flex: 1,
		marginVertical: 20,
		marginHorizontal: 10,
	},
});
export default NewAppointment;
