import { compose, graphql } from 'react-apollo';
import {
	ConversationMessagesQuery,
	CreateMessageMutation,
	NewMessageSubscription,
} from '../../../graphQL';
import ChatScreen from './ChatScreen';

export default compose(
	graphql(ConversationMessagesQuery, {
		options: (ownProps) => ({
			fetchPolicy: 'cache-and-network',
			variables: {
				conversationId: ownProps.navigation.state.params.conversationId,
			},
		}),
		props: (props) => ({
			loading: props.data.loading,
			error: props.data.error,
			messages: props.data.conversationMessages
				? props.data.conversationMessages.messages
				: [],
			subscribeToNewMessages: () =>
				props.data.subscribeToMore({
					document: NewMessageSubscription,
					variables: {
						conversationId:
							props.ownProps.navigation.state.params.conversationId,
					},
					updateQuery: (
						prev,
						{
							subscriptionData: {
								data: { subscribeToNewMessage },
							},
						}
					) => ({
						...prev,
						conversationMessages: {
							__typename: 'MessageConnection',
							nextToken: null,
							messages: [
								...prev.conversationMessages.messages.filter(
									(message) =>
										message.messageId !== subscribeToNewMessage.messageId
								),
								subscribeToNewMessage,
							],
						},
					}),
				}),
		}),
	}),
	graphql(CreateMessageMutation, {
		options: {
			fetchPolicy: 'no-cache',
		},
		props: ({ ownProps, mutate }) => ({
			onCreateMessage: (message) =>
				mutate({
					variables: message,
					optimisticResponse: {
						__typename: 'Mutation',
						createMessage: {
							__typename: 'Message',
							...message,
						},
					},
					update: (proxy, { data: { createMessage } }) => {
						const conversationId =
							ownProps.navigation.state.params.conversationId;
						const data = proxy.readQuery({
							query: ConversationMessagesQuery,
							variables: {
								conversationId,
							},
						});
						data.conversationMessages.messages = [
							...data.conversationMessages.messages.filter(
								(message) => message.messageId !== createMessage.messageId
							),
							createMessage,
						];
						proxy.writeData({
							query: ConversationMessagesQuery,
							variables: {
								conversationId,
							},
							data,
						});
					},
				}),
		}),
	})
)(ChatScreen);
