import React from 'react';
import { View } from 'react-native';
import Loader from '../../../components/Loader';
import Error from '../../../components/Error';
import Chat from './Chat';

class ChatScreen extends React.Component {
	static navigationOptions = {
		title: 'Chat',
		tabBarVisible: false,
	};
	subscription;
	componentDidMount() {
		this.subscription = this.props.subscribeToNewMessages();
	}
	componentWillUnmount() {
		this.subscription();
	}
	render() {
		const { loading, error } = this.props;
		if (loading) return <Loader />;
		if (error) return <Error error={`Error!: ${error}`} />;
		return (
			<View style={{ flex: 1 }}>
				<Chat {...this.props} />
			</View>
		);
	}
}

export default ChatScreen;
