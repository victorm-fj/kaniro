import { graphql } from 'react-apollo';
import {
	CreateMessageMutation,
	ConversationMessagesQuery,
} from '../../../graphQL';
import Chat from './Chat';

export default graphql(CreateMessageMutation, {
	options: {
		fetchPolicy: 'no-cache',
	},
	props: ({ ownProps, mutate }) => ({
		onCreateMessage: (message) =>
			mutate({
				variables: message,
				optimisticResponse: {
					__typename: 'Mutation',
					createMessage: {
						__typename: 'Message',
						...message,
					},
				},
				update: (proxy, { data: { createMessage } }) => {
					const conversationId =
						ownProps.navigation.state.params.conversationId;
					const data = proxy.readQuery({
						query: ConversationMessagesQuery,
						variables: {
							conversationId,
						},
					});
					data.conversationMessages.messages = [
						...data.conversationMessages.messages.filter(
							(message) => message.messageId !== createMessage.messageId
						),
						createMessage,
					];
					proxy.writeData({
						query: ConversationMessagesQuery,
						variables: {
							conversationId,
						},
						data,
					});
				},
			}),
	}),
})(Chat);
