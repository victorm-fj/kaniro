import React from 'react';
import { GiftedChat, Bubble, SystemMessage } from 'react-native-gifted-chat';

const formatMessage = (message) => ({
	_id: message.messageId,
	createdAt: new Date(Number(message.createdAt)),
	text: message.content,
	user: { _id: message.senderId },
});
class Chat extends React.Component {
	static getDerivedStateFromProps(nextProps) {
		if (nextProps.messages) {
			return { messages: nextProps.messages.map(formatMessage).reverse() };
		}
		return null;
	}
	state = {
		messages: [...this.props.messages.map(formatMessage).reverse()],
	};
	onSend = async (messages = []) => {
		let { _id, createdAt, text, user } = messages[0];
		const { conversationId } = this.props.navigation.state.params;
		const senderId = user._id;
		createdAt = createdAt.getTime().toString();
		const messageId = _id;
		const content = text;
		const message = { conversationId, createdAt, messageId, senderId, content };
		await this.props.onCreateMessage(message);
		this.setState((prevState) => ({
			messages: GiftedChat.append(prevState.messages, messages),
		}));
	};
	renderBubble = (props) => {
		return (
			<Bubble
				{...props}
				wrapperStyle={{
					left: {
						backgroundColor: 'rgb(170,170,170)',
					},
				}}
			/>
		);
	};
	renderSystemMessage = (props) => {
		return (
			<SystemMessage
				{...props}
				containerStyle={{
					marginBottom: 15,
				}}
				textStyle={{
					fontSize: 14,
				}}
			/>
		);
	};
	render() {
		return (
			<GiftedChat
				messages={this.state.messages}
				onSend={this.onSend}
				user={{ _id: this.props.screenProps.userId }} // sent messages should have same user._id
				renderBubble={this.renderBubble}
				renderSystemMessage={this.renderSystemMessage}
				renderAvatar={null}
			/>
		);
	}
}

export default Chat;
