// @flow
import moment from 'moment';
import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { Button, ListItem, Text } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';

const getDateString = (date: Date): string => {
	const isTodayNumber = moment().diff(date, 'hours');
	if (isTodayNumber === 0) {
		return moment(date).format('[Today] H:mm A');
	}
	return moment(date).format('ddd MMM D H:mm A');
};
type Props = {
	navigation: NavigationScreenProp<*, *>,
};
class Appointments extends React.Component<Props> {
	static navigationOptions = {
		header: null,
	};
	onPressNewAppointment = () => {
		this.props.navigation.navigate('NewAppointment', { user: this.props.user });
	};
	onPressAppointment(appointment) {
		this.props.navigation.navigate('Details', { appointment });
	}
	renderItem = ({ item }) => {
		if (item.pet) {
			return (
				<ListItem
					title={`Pet: ${item.pet.name}`}
					subtitle={`Date: ${getDateString(new Date(Number(item.date)))}`}
					chevron
					chevronColor="rgb(117,117,117)"
					onPress={() => this.onPressAppointment(item)}
				/>
			);
		}
		return <Text h3>No pets</Text>;
	};
	render() {
		const { appointments } = this.props.user;
		return (
			<View style={styles.container}>
				<FlatList
					style={{ flex: 1 }}
					data={appointments}
					extraData={this.props.user}
					keyExtractor={(item, index) => String(index)}
					renderItem={this.renderItem}
				/>
				<Button
					title="New Appointment"
					onPress={this.onPressNewAppointment}
					buttonStyle={styles.button}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	list: {
		flex: 1,
	},
	button: {
		margin: 20,
		backgroundColor: 'rgb(88,86,214)',
		borderRadius: 5,
	},
});
export default Appointments;
