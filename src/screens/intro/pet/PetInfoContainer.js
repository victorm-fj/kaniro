import { graphql } from 'react-apollo';
import { AddPetMutation, MeQuery } from '../../../graphQL';
import PetInfo from './PetInfo';

export default graphql(AddPetMutation, {
	props: ({ ownProps, mutate }) => ({
		onAddPet: (pet) =>
			mutate({
				variables: pet,
				optimisticResponse: {
					__typename: 'Mutation',
					addPet: {
						__typename: 'Pet',
						...pet,
						image: {
							__typename: 'Image',
							...pet.image,
						},
					},
				},
			}),
	}),
	options: (props) => ({
		fetchPolicy: 'cache-and-network',
		refetchQueries: [
			{
				query: MeQuery,
				variables: { userId: props.screenProps.userId },
				fetchPolicy: 'cache-and-network',
			},
		],
		update: (proxy, { data: { addPet } }) => {
			const data = proxy.readQuery({
				query: MeQuery,
				variables: { userId: addPet.userId },
			});
			data.me.pets = data.me.pets
				? [...data.me.pets.filter((pet) => pet.petId !== addPet.petId), addPet]
				: [addPet];
			proxy.writeQuery({
				query: MeQuery,
				variables: { userId: props.screenProps.userId },
				data,
			});
		},
	}),
})(PetInfo);
