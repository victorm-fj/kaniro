// @flow
import React from 'react';
import {
	StyleSheet,
	View,
	ScrollView,
	Platform,
	DatePickerAndroid,
} from 'react-native';
import { Avatar, Input, Button, ButtonGroup } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import moment from 'moment';
import uuidV4 from 'uuid/v4';
import S3 from 'aws-sdk/clients/s3';
import { Auth } from 'aws-amplify';
import awsmobile from '../../../aws-exports';
import ImagePicker from '../../../components/imagePicker/ImagePicker';
import CustomDatePicker from '../../../components/customPicker/CustomDatePicker';
import Loader from '../../../components/Loader';

const grayPaw =
	'iVBORw0KGgoAAAANSUhEUgAAAhQAAAIFCAMAAAC9LpTpAAAAP1BMVEVHcEyoqKeoqKeoqKeoqKeoqKeoqKeoqKeoqKeoqKeoqKf///+oqKe9vbzt7ez4+PjW1tXLy8qtra3i4uGzs7KiuhqJAAAAC3RSTlMAgBDwZafARSnZkCuXa3gAABN3SURBVHja7J3dmqQoE4TH7iqtKgREvP9r/XZn59vt7vpTSCBSIw7noKYffc2MDFB+/aIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoiqIoqoE+hmHouu7c/6XP8U5//3Pfdd1pGC68WDvXZTh1t74ft+nan7vTQDr2VxlOXX8d83Ttbx3Z2IWG0+1Rh0hXf+6GD15XtTx05+tYRteeZOhzD6diPHwh43xiN9EDxOdYRfPf3aQbeMkJxAOb0bFiwI4YFVrGM32ylSCWiH5sreuNXAARcbuOGLqe6TBIxKNGQi5IBLmAIqJDJOIPF/QXLWaNEy4Rf/wF55G6GlrEEdt1PvFWsUiwjTRyEudRl+g6S0tRkfjqLriiWq5vdBqRGMd5/DyzixTqG5+jYrGLlJg3Ru3qiYUsEv24B105ohIJYkEkiAWRIBaNJo79IfEbC1rO44SXnETKR1XjnsU4KyXQ/hx3rhvD743+8jruX590nPSXdJw0E7QW7BypPaTjLT/wGMoewpmDc4hQmbiOxxSLxVN141E1j2cWC5aJe8PJYsEy8WA6ZbFgmWCxeDN0kAiOIT8izJ44/H8MYcD5J8L8JAz/iYtkf+tGEL6pZwuhw6TfZOtYo46tg2IL+TJ1sHVwCvlpJ9g6OIUwsNqWeh+QiTNv+7sW8kE7Qd3NpscyFkwn1lFxJGPBdIJ2kxaTdpMWkznWVotJJjiEcOzgEEImSAWTbY6mZIKjKZkgFYysSAUjK1W6kQnqAOEmmSAVZIJUkAlSQSZIBZnA0V5eCbnwVjKvYI5JKsgEqSATzaX9BWTunygg5fsryASpuBO/W1RGmvdtco8uqfgpfhqT0SaDTFLBIJNxBQMKxhUcRjmYcvDgCMLBYxfqVTEx8IZV0Y0mk1JsNmkyaTZpMmk2mWQy2WSSyWSThkKdLjQUlD5bQUNBW8GEgraChoJpxWbxpJ9GtoJLHtSdcF8x/aCh4FzKaZRzKZsHsm5sHtSdBjYPSkMDYfNgA2Hz4ATC2IoRFpsHI6x8cc0DQVhrIHzNA0NIL4JwwZxhxZ34wRoQzThhBV0mvaYml7mE6MxvuckyrKDLHOfwB4g/8tNMr3lslzlP3vyUn+g1j7wQZu+R+I3F7pvIlS7zmSbzTALF4l+n4hGdyonj6OPW4cxzxUxn8cOpuICWa36wUGxlwhiXQcVDpwJWLTqOo5uZyLmLwZdqSZKlovFYCvmWYDSmDBVPzGtm8ZHXmYXi7mk27+WXlM5RqCXtLMFCzK0WvwKKhJtoyxmVXZUKyD140aySk3ET6T+41wQLsVBYs1JRuichuc2eheKr3FooTJAuP4GlQnmh2DSCTOt+cGap0OwotrmAsPIHJ5YKyCVzvwGKtTdxffVZjl4qIAtFMJu06iZaL03ZfrMKpWHm5gYybyg+SK6iSVYBuephNmqF13Rbfi8cu1RAFgq7FYr3pWISLz07LhXXHXSPFY/2VsyArOZYfV8F5oYrb4RLxeyFIaup6vsq+l10j7euYpLvR/X0WXsLN2ShCAlQRGHKkBZLK+/WxNzCHROgeOkCnHTlqau6G7s/IJlIsBSvA6cg+3M7z7oxXwpLsRTGeDmXCbet4nz4eTTpwX5V8CdZxnYeYKl7ASjJaiYVCmOgLkl3dJuZYgtfPduTcOHZ91SKenRc2pP97DYmFgosKOpNpajfHkhk4snAEER/rZX6Y9vMcUmFwonWHbCvHVwObTMTJ9JnKWTyr0Wsq9Id2mZmQGGl0lG4oKJWqgmaZiZPC48r/mx2AkWdVPO0PyicnM3Eg+J8YJuZA4URyzwAoajxDRPc8+0zoLBikwweFDWiiu4QUIQdQXE+cPfIgWISmz0AoSi/VxO3e+RAEaWSK0goiveP2y6hcGKRB1x4VSOquO4SCi9nKQzgR30vh+0eOVAYOUuBCMXpsN0j6/FexCyFAfzO8/mw3SPLCFihjBsTirLzB3L3EIQi55cM4qUp2j9OyFCMYkYgpxF5xCtTtH/0x4Aix7I6xCtTcqvmBzQTGYtYP8IFt6/hYyy6fo7dPeSy6RwoAuSluR1vz1W+FYhSfchgnjxUMNQEP5PWShX9vQ0fJUPNywguISjmvfnMkkMp/InmTgYKuzufWXAohT+9emoPBephhsWGUnQmMjbRWSEoPOy1KTSUKjjS3LeGIsJemu6gliIjqZCCIsBemv6gliLjdkqNtsCHqR/UUmT0DyEoIvClGQ5qKZLnDycERQC+NN1BLUXy/RSCwiNfmv6gliI5v5pkoEDuHmWSik8VUASBsp8MxQJ9aS4HXPjIsZqLCBQO+8oUWP44KYFiyvcCs0TYcYjlj6Z7KWY7ud9uwcXpzaVP+ajdTy+wy0JRYk9Fw8399kdQGYNwqQgiaYdFL6I7iq4eHTzvp0WyVCwSIwx6oSgQX7WKruZnyxlxERtAvEgEBl8o5J1mI5/56kzQaRYaQCaJCCzCMyHvNG9wTDw/rDz7MLiE8cMv+FBcd5FnLu+e+SfFIuaaAZftVQ/hNPHqxD83dEmC6Z0ZmPbnMgtkmi3yzHnN8/q4hYQcm5nQgLxVAcVJ//AxZRRul1n4/Q6bh/jqeYN1c5tzR9Y3ECeQgEUdTEivnjcIuV3Wcxry4oVN84eblUBx1T58hMzcKCZmFNsHGCWGQn78qP/3+0yft8qmGv/sIV/ykDzC+FH/wxTb/P+je2t91g2ddmYy5Vc/6g8fMT8nsOnNY8O62iomZvuXADLPTvXKx+ageUqxJTHf1Lxnwk7/NjI3NW41N9UT6fatlnZ7JPpmaogCTNj440/wTbtNr3oi3f7+n9sclL+bJFc0kNd3eA4ebFi5qp5Ipb4M8WIGiW/ThXem5PVTP08++X9WMZPW3ouXtJ/6sZELPv3rIiEjnwgeMe36UBxTJG19erJS+XDvlltXw19R8fJ5Xxxm3jUojinS3vOyawt5XH1PnpqS160jvLMjrWrFoDimSNtO/eJVThviH9BcDFsCA+u2bQVca5MbbcDoFEOB9NL3/VP/ai/56oB90g5F7Zgi9b29Mm99z+HrXfbxDXqzy2t2RXU+HhTF1iFmO0XnXJzC21tpfZ4vVpNe3bRA0f4DEdY3J7gSFLWzq/TvYbZestyyZbgJwUeEovGu6tmhE6w30Mw4hGXRw0QTgg8WaLac9NbnE60JvqiFIuPgjpZWMyggeFALhc4vplsNBA9qlz5UHjWe8gmdRS8UgyYomvWPqGKCPqmFQuMhXlZHWev0QpFz1vikpnm0KGuKocg5A7JNfpU4Rc+EokZQ0ea8v0VLrxODov73rqw2U5F6+kxQC0Wn5sFrZSoWNX/rWS8UWU4z6ikU9aHoFUMRVTnNxRAK7NWPBk5T0VK/ZigWVU7TEwr4pMLqKWuEolZSUbtRR0NPAZ9UVL7SsyEU+ENpVNM9CEW1/uHUdI/6iaZuKJZqlcKGaXLOTdOU9lkqVYOSbigy5o8NNfnLB6n+Wcve/lmqrHWahVBUatVrb+sSHxoXv40LXSu6yqFIt5rrNim8/NRMqFHRWiTyYlA0Oioq9RFcs53p+QepVn2URMpScDve5vnfF7vSYcVPr8Qiy1IEQlGpVLz1mXZlxV/1YaygavjQD8Vc5NDYOYrOMTk+s8HrCOqhSLzgS3bn+FIs3s6MTpWl2AEUSQOIf20wt/5aKAhFUAzFpRkU0qfGWiduUVRFV5pfMM5qIEGqdfzbQl7lHjlLpC3eUNkDFAKn2Sc5zO8txJaZSAOhqNRAnlqKJb37v6DC6uoegh8tuY56GsgzC2Bz3hp4/lAHXd1D8YfQvmlj0bcFMqYXrE26usdeoNj2dTFXYCnz1VCT/sN+1g3FbWxKhc99/OZo8vX4KIdJmjIli6TN1s4T7IDPrzXbRtOgymbuCIoN37G15Zh4TIVVZTMlP9g+jK2pcOlXevHGlKMiGYo2X2HqdgTF2kOqF+lR9D0VqgqFJBSXsb1i2tMnysQjKjQ5CtlzrQGgWLFwEYoz8YAKp2f0EIbiE4GKd0ucsQIT91QkTbu+1RmUl187Sa9WhgKhChN3biApqGh2LInkYbVnDCheTSG2EhM/K5LV4zKFj7XuRhQ96SGP4kaxfOI1FQnNo9mhJL0kFKdxRMbCFc2s3lT/qKd5jDdJKIYRScu3MyHdk1NCyzHxvVcFLZOHbEzR4HCH9/UiTL9l54xcQ2TXzazFUAhPpBhBhdygIkDFnIpfs2lUeiKFmUnXJ12msFzi/OFty8siygTMTLq2uZjiiknupS0TV1koOlVMLL48FF9mCKuECdmJFG38EFlSFTSbUQcTssMHxjqp6IKq6CrIyg2Dbml8YU6yUGgaP4KppLitgbi59ZUZhKHQM35YU01hC4hT+0sjzISe8WP29aD4sogRwe1EgeEDa/UDwVDcpRWv07JpBrg0Z2kotIwfwVRVWPU/OwtxbTppKJQ4TevrQvF1FfzZzjAQJOR9ZtuXjNfLmcr6trz1aBtptDAX5+PXIZ3mZKrr++YI+83S+BhmnIsj7jN1OE1bn4n7VU8bJueci1OwWFfnLA+Fgkxz9g2gaLlnpm2eqcNpTqaJrBIoLgWg6Nk8njQQJVAUYAJ/9dw1gqLhRtwt6ktAMbB5IO6waxhdQW7e/abFtNOkAYqhBBTg8ZVrCIVZFEBRhInGX74CdZlqxtK+DBTIpqJNRKGpVHRloEA2FVNbJhSUijKWAjmpWExrwZeKQkwAJxWuORTopaIvBQWsqbDGsFTUX/gA+srRA3kAKMBLxaUYFKB7KgIAE+Cx5rUYE6h7KhAKBXiseS4HBeaeigmCCezF0qEcFJBJ94xRKLAXSz9+Hat/gBSKtp+oadc9IPsHTKFAnkpPJaEA7B8Bhglgq3kpCgXeSilOocC1mteiTOD1D6BCgWs1y3YPvP7hkKCIh+wecP3DGijNR+wecP0jYkERDtk9wPoH0DwKHFVcikMB1T+CMewf79QXZwKrfzg0KMIhuwdU/1jQmICcPz4qQHFi91DVP84VmEDqHw4PCrxX0IcaUODsv8LrHoD947MKEzj7dwG7B976x60OFDD7dyMgFHDr55dKUKBEFR4Riv+1dy7IjcJAEJUMCGCDDCb3P+sm2U8ldhwbsDUteH2AVKp41dPTElhsKQ2JmFCJmr0iE2qXKspUUIi8PzhKQqHVdBfJmBCpKiagkImZMlEzSkKhtX40CaGQeNX4l6Z2GTNVomYPFCJtplCrCRQ31SZlQqHVHIBCZx9V2UpFoRAKmsUhMRQlUMivpJ1LrRYo1KFokkNRAoV4zV0lZ8IdCqDQPhBLbxT2BZboSipzdB4MmDC3ilcihVBxpWIVXKeQMwp7q5gwCjmjMLcKxVDR79wo7K0iso/KGQVWoXu/38wo7K1CrKqIJ4zC3ipOESbUjMLeKh5FRZyGof/QMEwx99lhaxT2XcV6KuIwXpSQ/Th73T0qve9ROVtZH5ae1rQVx2m82kr3s/5wlHqxuDGGwv5exXhcCsSNB3m6/y9rvQFkbRQK3zB5nR4PxD+7iPnZRPoLV5rvoPfxMSPjO+KGY05p4l3e2UvicxX3uUUc+gVrYz/9xEVUe828FWBC5XXj0xhvLBkrTP4qF1HvyzWlAhQ6v0x5Gr97dsd1PPznYoiXk0gPCdveSuhi3hcw+nGY4l9Nbzg8sGZ8HT+BEYf+RVGNBhSqvzf3rFT7Ltn/rnMqCi+IdVQzayKRlPlHHY+DlHmRNVseCClTsddE3mmp4pHYd5kHMSikyoqdqnZqKnkoVBSUFQyPO8oKBgjDgwHC8GCAMDzYQBgeVFgMDyoshgdnIBtX45TFALFQ6bTleUTJVTl1sZemVnGQh4K9lG2UWGEt73IQsSKlgstDxAoaCtoKGgqu/NNQcIpOQ8EhSE6BwuUlYgWtFWHTgInG5SaubBIyaTZpMllBWDxYQVg8oIJ2mxWExQMqYILFdHOqnYMKlH1BARUwQYlFaUWJRWkFFTABFTABFTABFTABFTABFTABFTABFTABFWgPTDhXcw6y4/MOTsdgAiqeqmLbTHAXawkTjXNQgXbGhOOO9zxlfG97lvhtwhmr6E6YoLCgnqCwYBW9czUlbt4RMWu3Lx2ImzcjZuN2Jz63ScQkWMyTd/sUwYI4QbAgTtBY0E4wQjgUZYQwOp46QthCPqkDCEYIWwfnphRWM4oszIKEeZk3MYtAwsQssAnMApvALLAJzGLd0oFN0FmcVZh0ExSc5xUm3QSnIQRMAufPk4OAyQw5k2dyzJsh27/Xy86xYA/ZdrQI7BzLokVgDUV7SZzky5WJc3tYFORLsAAJsAAJsAAJNhHipRYWefcWASSeVGdlW35X9BLPK7+zDBdFR6FNuDiLEqTLBFOkK5gb6HJFDZgEurQLr54uCo7GTXbUgrGBcukuKsaG8ZJaQQSSniMQocRF1wokyxoi1PYR00HSenYNDOMzEB0WIZ4804LRViUWkYlj+JAgewYcIr+M0QUMAn1DRu3DY6dJETp42MQ0KX213jXa4EvmxeYiaO27EBbAUPmyxh22PlPq0r/zcZWQNoRQee9rWNg1Jh8CAYQQQgghhBBCCCGEEEIIIYQQQgghhBBCi/QbbgYesDA5abYAAAAASUVORK5CYII=';
const genderValues = ['male', 'female'];
type ImageType = { type: string, data: string };
type Props = {
	navigation: NavigationScreenProp<*, *>,
	screenProps: NavigationScreenProp<*, *>,
};
type State = {
	image: ImageType,
	name: string,
	breed: string,
	birthDate: Date,
	adoptedDate: Date,
	selectedGenderIndex: number,
	pickerBirth: boolean,
	pickerAdopted: boolean,
	loading: boolean,
};
class PetInfo extends React.Component<Props, State> {
	static navigationOptions = {
		headerTransparent: true,
		headerStyle: { borderBottomWidth: 0 },
	};
	state = {
		image: { type: 'png', data: grayPaw },
		name: '',
		breed: '',
		birthDate: new Date(),
		adoptedDate: new Date(),
		selectedGenderIndex: 0,
		pickerBirth: false,
		pickerAdopted: false,
		loading: false,
	};
	async uploadImage(key: string, mimeType: string, buffer: Buffer) {
		let credentials = await Auth.currentCredentials();
		credentials = Auth.essentialCredentials(credentials);
		const s3 = new S3({
			apiVersion: '2006-03-01',
			credentials,
		});
		const params = {
			Bucket: awsmobile.aws_user_files_s3_bucket,
			Key: key,
			ContentType: mimeType,
			Body: buffer,
		};
		return new Promise((resolve, reject) => {
			s3.putObject(params, (error, data) => {
				if (error) {
					console.log('putObject error -', error);
					reject(error);
					return;
				}
				console.log('putObject data -', data);
				resolve(data);
			});
		});
	}
	showAndroidDatePicker = async (stateValue: string) => {
		try {
			const { action, year, month, day } = await DatePickerAndroid.open({
				date: new Date(),
			});
			if (action !== DatePickerAndroid.dismissedAction) {
				this.setState({ [stateValue]: new Date(year, month, day) });
			}
		} catch (error) {
			console.log('showAndroidDatePicker error -', error);
		}
	};
	addImage = (image: ImageType) => {
		this.setState({ image });
	};
	setSelectedBirthDate = (birthDate: Date) => {
		this.setState({ birthDate, pickerBirth: false });
	};
	setSelectedAdoptedDate = (adoptedDate: Date) => {
		this.setState({ adoptedDate, pickerAdopted: false });
	};
	onPressGender = (selectedGenderIndex: number) => {
		this.setState({ selectedGenderIndex });
	};
	onPressBirth = () => {
		if (Platform.OS === 'ios') {
			this.setState({ pickerBirth: true });
		} else {
			this.showAndroidDatePicker('birthDate');
		}
	};
	onPressAdopted = () => {
		if (Platform.OS === 'ios') {
			this.setState({ pickerAdopted: true });
		} else {
			this.showAndroidDatePicker('adoptedDate');
		}
	};
	onChangeValue = (key: string, value: string) => {
		this.setState({ [key]: value.trim() });
	};
	onPressContinue = async () => {
		this.setState({ loading: true });
		const {
			image: { type, data },
			name,
			breed,
			selectedGenderIndex,
			birthDate,
			adoptedDate,
		} = this.state;
		const userId = this.props.screenProps.userId;
		const petId = uuidV4();
		const imageBuffer = new Buffer(data, 'base64');
		const mimeType = type === 'jpg' ? 'image/jpeg' : `image/${type}`;
		const visibility = 'public';
		const key = `${visibility}/${userId}/${petId}.${type}`;
		await this.uploadImage(key, mimeType, imageBuffer);
		const pet = {
			userId,
			petId,
			name,
			breed,
			gender: genderValues[selectedGenderIndex],
			birthDate: birthDate.getTime().toString(),
			adoptedDate: adoptedDate.getTime().toString(),
			image: {
				bucket: awsmobile.aws_user_files_s3_bucket,
				key,
				region: awsmobile.aws_user_files_s3_bucket_region,
			},
		};
		await this.props.onAddPet(pet);
		// setTimeout(() => {
		this.setState({ loading: false });
		const { state } = this.props.navigation;
		if (state.params && state.params.from === 'home') {
			this.props.navigation.goBack();
		} else {
			this.props.navigation.navigate('Main');
		}
		// }, 2000);
	};
	getButtonTitle() {
		const { state } = this.props.navigation;
		if (state.params && state.params.from === 'home') {
			return 'Add pet';
		}
		return 'Continue';
	}
	render() {
		const {
			name,
			breed,
			pickerBirth,
			pickerAdopted,
			image,
			birthDate,
			adoptedDate,
			loading,
		} = this.state;
		return (
			<View style={styles.container}>
				{loading && <Loader />}
				{pickerBirth && (
					<CustomDatePicker
						setSelectedDate={this.setSelectedBirthDate}
						mode="date"
					/>
				)}
				{pickerAdopted && (
					<CustomDatePicker
						setSelectedDate={this.setSelectedAdoptedDate}
						mode="date"
					/>
				)}
				<ScrollView style={styles.content}>
					<View style={styles.avatar}>
						<Avatar
							size="xlarge"
							rounded
							source={{ uri: `data:image/${image.type};base64,${image.data}` }}
						/>
						<ImagePicker
							addImage={this.addImage}
							style={styles.avatarIcon}
							size={40}
						/>
					</View>
					<View style={styles.form}>
						<Input
							placeholder="Your dog's name"
							containerStyle={styles.input}
							value={name}
							onChangeText={(value) => this.onChangeValue('name', value)}
						/>
						<Input
							placeholder="Your dog's breed"
							containerStyle={styles.input}
							name={breed}
							onChangeText={(value) => this.onChangeValue('breed', value)}
						/>
						<ButtonGroup
							containerStyle={styles.input}
							buttons={['Male', 'Female']}
							onPress={this.onPressGender}
							selectedIndex={this.state.selectedGenderIndex}
						/>
						<Button
							containerStyle={styles.input}
							buttonStyle={{
								backgroundColor: 'transparent',
								justifyContent: 'flex-start',
							}}
							titleStyle={{ color: 'rgb(21,21,21)' }}
							title={`Birth date: ${moment(birthDate).format('MMM D YYYY')}`}
							onPress={this.onPressBirth}
						/>
						<Button
							containerStyle={styles.input}
							buttonStyle={{
								backgroundColor: 'transparent',
								justifyContent: 'flex-start',
							}}
							titleStyle={{ color: 'rgb(21,21,21)' }}
							title={`Adopted date: ${moment(adoptedDate).format(
								'MMM D YYYY'
							)}`}
							onPress={this.onPressAdopted}
						/>
					</View>
				</ScrollView>
				<Button
					containerStyle={styles.buttonContainer}
					buttonStyle={styles.button}
					title={this.getButtonTitle()}
					onPress={this.onPressContinue}
					selectedButtonStyle={styles.button}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		paddingTop: 64,
	},
	content: {
		flex: 1,
	},
	title: {
		fontSize: 24,
		textAlign: 'center',
	},
	avatar: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	avatarIcon: {
		position: 'absolute',
		bottom: 20,
		right: '30%',
	},
	form: {
		flex: 2,
	},
	input: {
		marginTop: 20,
	},
	buttonContainer: {
		marginTop: 20,
	},
	button: {
		backgroundColor: 'rgb(88,86,214)',
	},
});
export default PetInfo;
