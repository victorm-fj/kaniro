import { graphql } from 'react-apollo';
import { NewUserMutation, MeQuery } from '../../graphQL';
import Welcome from './Welcome';

export default graphql(NewUserMutation, {
	props: ({ ownProps, mutate }) => ({
		onNewUser: (user) =>
			mutate({
				variables: user,
				optimisticResponse: {
					__typename: 'Mutation',
					newUser: {
						__typename: 'User',
						userId: user.userId,
						registered: false,
						pets: null,
					},
				},
			}),
	}),
	options: (props) => ({
		fetchPolicy: 'cache-and-network',
		refetchQueries: [
			{
				query: MeQuery,
				variables: { userId: props.screenProps.userId },
				fetchPolicy: 'cache-and-network',
			},
		],
		update: (proxy, { data: { newUser } }) => {
			const data = proxy.readQuery({
				query: MeQuery,
				variables: { userId: props.screenProps.userId },
			});
			data.me = data.me ? { ...data.me, ...newUser } : newUser;
			proxy.writeQuery({
				query: MeQuery,
				variables: { userId: props.screenProps.userId },
				data,
			});
		},
	}),
})(Welcome);
