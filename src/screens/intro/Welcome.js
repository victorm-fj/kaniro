// @flow
import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import branch from 'react-native-branch';

const kaniro = require('./Kaniro.png');
type Props = {
	navigation: NavigationScreenProp<*, *>,
};
class Welcome extends React.Component<Props> {
	static navigationOptions = {
		header: null,
	};
	onPressHandler = async () => {
		const userId = this.props.screenProps.userId;
		await this.props.onNewUser({ userId });
		branch.setIdentity(userId);
		this.props.navigation.navigate('PetInfo');
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.content}>
					<Image source={kaniro} style={styles.logo} resizeMode="contain" />
					<Text style={styles.title}>
						Keep track of your pet's data and health
					</Text>
				</View>
				<Button
					title="Start"
					containerStyle={styles.buttonContainer}
					buttonStyle={styles.button}
					onPress={this.onPressHandler}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
	},
	content: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	logo: {
		width: 120,
		height: 120,
		marginBottom: 40,
	},
	title: {
		fontSize: 24,
		textAlign: 'center',
	},
	buttonContainer: {
		marginTop: 20,
	},
	button: {
		backgroundColor: 'rgb(88,86,214)',
	},
});
export default Welcome;
