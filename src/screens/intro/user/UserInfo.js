// @flow
import React from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
import { Button, Input } from 'react-native-elements';
import { NavigationScreenProp } from 'react-navigation';
import uuidV4 from 'uuid/v4';
import { Buffer } from 'buffer';
import awsmobile from '../../../aws-exports';

type Props = {
	navigation: NavigationScreenProp<*, *>,
};
type State = {
	firstName: string,
	lastName: string,
	email: string,
	password: string,
};
class UserInfo extends React.Component<Props, State> {
	static navigationOptions = {
		headerTransparent: true,
		headerStyle: { borderBottomWidth: 0 },
	};
	state = {
		firstName: '',
		lastName: '',
		email: '',
		password: '',
	};
	onChangeValue = (key: string, value: string) => {
		this.setState({ [key]: value });
	};
	onPressContinue = async () => {
		const {
			state: {
				params: { pet },
			},
		} = this.props.navigation;
		const { firstName, lastName, email } = this.state;
		const { type, data } = pet.image;
		const imageBuffer = Buffer.from(data, 'base64');
		const userId = this.props.screenProps.userId;
		const petId = uuidV4();
		const visibility = 'public';
		const mimeType = type === 'jpg' ? 'image/jpeg' : `image/${type}`;
		const petObj = {
			...pet,
			userId,
			petId,
			image: {
				bucket: awsmobile.aws_user_files_s3_bucket,
				key: `${visibility}/${userId}/${petId}.${type}`,
				region: awsmobile.aws_user_files_s3_bucket_region,
				localUri: imageBuffer,
				mimeType,
			},
		};
		const user = {
			userId,
			firstName,
			lastName,
			email,
		};
		console.log('user object -', user);
		await this.props.onNewUser(user);
		console.log('pet object -', petObj);
		await this.props.onAddPet(petObj);
		this.props.navigation.navigate('Home');
	};
	render() {
		const { firstName, lastName, email, password } = this.state;
		return (
			<View style={styles.container}>
				<View style={styles.content}>
					<Text style={styles.title}>
						For the last step please provide the next information
					</Text>
					<ScrollView style={styles.form}>
						<Input
							placeholder="First name"
							value={firstName}
							onChangeText={(value) => this.onChangeValue('firstName', value)}
							containerStyle={styles.input}
						/>
						<Input
							placeholder="Last name"
							value={lastName}
							onChangeText={(value) => this.onChangeValue('lastName', value)}
							containerStyle={styles.input}
						/>
						<Input
							placeholder="Email"
							value={email}
							onChangeText={(value) => this.onChangeValue('email', value)}
							containerStyle={styles.input}
						/>
						<Input
							placeholder="Password"
							value={password}
							onChangeText={(value) => this.onChangeValue('password', value)}
							containerStyle={styles.input}
						/>
					</ScrollView>
				</View>
				<Button
					containerStyle={styles.buttonContainer}
					buttonStyle={styles.button}
					title="Continue"
					onPress={this.onPressContinue}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20,
		paddingTop: 64,
	},
	content: {
		flex: 1,
	},
	title: {
		fontSize: 24,
		textAlign: 'center',
	},
	form: {
		flex: 1,
		paddingTop: 40,
	},
	input: {
		marginTop: 20,
	},
	buttonContainer: {
		marginTop: 20,
	},
	button: {
		backgroundColor: 'rgb(88,86,214)',
	},
});
export default UserInfo;
