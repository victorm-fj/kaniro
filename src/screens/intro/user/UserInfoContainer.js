import { compose, graphql } from 'react-apollo';
import { NewUserMutation, AddPetMutation, MeQuery } from '../../../graphQL';
import UserInfo from './UserInfo';

export default compose(
	graphql(NewUserMutation, {
		options: {
			fetchPolicy: 'no-cache',
		},
		props: ({ ownProps, mutate }) => ({
			onNewUser: (user) =>
				mutate({
					variables: user,
					optimisticResponse: {
						__typename: 'Mutation',
						newUser: { __typename: 'User', ...user, registered: false },
					},
					// update: (cache, { data: newUser }) => {
					// 	const data = cache.readQuery({
					// 		query: MeQuery,
					// 		variables: { userId: ownProps.screenProps.me.userId },
					// 	});
					// 	console.log('cacheData -', data);
					// 	data.me = { ...data.me, ...newUser };
					// 	cache.writeData({ query: MeQuery, data });
					// },
				}),
		}),
	}),
	graphql(AddPetMutation, {
		options: {
			fetchPolicy: 'no-cache',
		},
		props: ({ ownProps, mutate }) => ({
			onAddPet: (pet) =>
				mutate({
					variables: pet,
					optimisticResponse: {
						__typename: 'Mutation',
						addPet: {
							__typename: 'Pet',
							...pet,
							image: null,
						},
					},
					// update: (cache, { data: addPet }) => {
					// 	const data = cache.readQuery({
					// 		query: MeQuery,
					// 		variables: { userId: ownProps.screenProps.me.userId },
					// 	});
					// 	data.me.pets = [addPet];
					// 	cache.writeData({ query: MeQuery, data });
					// },
				}),
		}),
	})
)(UserInfo);
