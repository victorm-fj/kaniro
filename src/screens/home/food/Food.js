import React from 'react';
import { FlatList } from 'react-native';
import { iOSColors } from 'react-native-typography';

import Card from '../../../components/card';
import FoodItem from '../components/FoodItem';
import SwipeOut from '../../../components/swipeOut/SwipeOut';

const rightButtons = [
	{
		title: 'Active',
		titleColor: iOSColors.white,
		iconName: 'ios-checkmark-circle',
		iconSize: 30,
		backgroundColor: iOSColors.green,
	},
	{
		title: 'Delete',
		titleColor: iOSColors.white,
		iconName: 'ios-trash',
		iconSize: 30,
		backgroundColor: iOSColors.blue,
	},
];
const data = [
	{
		id: 0,
		title: 'Alfo\'s tailor-made blend',
		calPercentage: 100,
		foodQuantity: '2 cups (538 g)',
		completed: true,
	},
	{
		id: 1,
		title: 'Carla\'s Kaniro 80/20',
		calPercentage: 100,
		foodQuantity: '1 cup (250 g)',
		completed: true,
	},
];
const total = {
	totalQuantity: 842,
	measure: 'g',
	remainingDays: 17,
	recommendedQuantity: 0,
};

class Food extends React.Component {
	renderItem = ({ item }) => {
		return (
			<SwipeOut rightButtons={rightButtons}>
				<Card.Content divider style={{ backgroundColor: iOSColors.white }}>
					<FoodItem foodItem={item} />
				</Card.Content>
			</SwipeOut>
		);
	};
	render() {
		return (
			<Card>
				<Card.Header divider title="FOOD" buttonTitle="ADD FOOD" icon />
				<FlatList
					data={data}
					extraData={data}
					keyExtractor={(item, index) => String(index)}
					renderItem={this.renderItem}
				/>
				<Card.Footer total={total} />
			</Card>
		);
	}
}

export default Food;
