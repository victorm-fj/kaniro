import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { human, iOSColors, systemWeights } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Greeting = () => {
	return (
		<View style={styles.container}>
			<Icon
				name="ios-camera"
				type="ionicon"
				raised
				color={iOSColors.gray}
				size={26}
				onPress={() => {}}
			/>
			<TouchableOpacity style={styles.message}>
				<Text style={[human.subheadWhite]}>Hello.</Text>
				<View style={styles.action}>
					<Text style={[human.subheadWhite, { paddingRight: 10 }]}>
						What's your <Text style={[systemWeights.bold]}>Name?</Text>
					</Text>
					<Ionicons
						name="ios-arrow-forward"
						size={22}
						color={iOSColors.white}
					/>
				</View>
			</TouchableOpacity>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		width: '90%',
		flexDirection: 'row',
		marginTop: 20,
	},
	message: {
		marginLeft: 15,
		justifyContent: 'center',
	},
	action: {
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default Greeting;
