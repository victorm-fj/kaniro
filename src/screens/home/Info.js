import React from 'react';
import { StyleSheet, View, Share, Image } from 'react-native';
import { Text, Button } from 'react-native-elements';
import ActionSheet from 'react-native-actionsheet';
import { Auth } from 'aws-amplify';
import GridView from 'react-native-super-grid';
import moment from 'moment';
import RNRestart from 'react-native-restart';
import S3 from 'aws-sdk/clients/s3';
import { getReferralLink } from './helpers';
import { asyncForEach } from '../../utils/helpers';
import Loader from '../../components/Loader';

const colorPalette = [
	'#1abc9c',
	'#2ecc71',
	'#3498db',
	'#9b59b6',
	'#34495e',
	'#16a085',
	'#27ae60',
	'#2980b9',
	'#8e44ad',
	'#2c3e50',
	'#f1c40f',
	'#1abc9c',
	'#2ecc71',
	'#3498db',
	'#9b59b6',
	'#34495e',
	'#16a085',
	'#27ae60',
	'#2980b9',
	'#8e44ad',
	'#2c3e50',
	'#f1c40f',
];

class Info extends React.Component {
	state = { loading: true, sources: null, newPet: false };
	// async componentDidMount() {
	// 	const { pets } = this.props.user;
	// 	const sources = {};
	// 	await asyncForEach(pets, async (pet) => {
	// 		const {
	// 			image: { key, bucket },
	// 		} = pet;
	// 		const url = await this.getSignedUrl(bucket, key);
	// 		sources[key] = url;
	// 	});
	// 	this.setState({ sources, loading: false });
	// }
	// async componentDidUpdate(prevProps, prevState) {
	// 	const prevPets = prevProps.user.pets;
	// 	const { pets } = this.props.user;
	// 	if (prevPets.length !== pets.length) {
	// 		this.setState({ loading: true, newPet: true }, async () => {
	// 			if (this.state.newPet) {
	// 				const sources = {};
	// 				await asyncForEach(pets, async (pet) => {
	// 					const {
	// 						image: { key, bucket },
	// 					} = pet;
	// 					if (prevState.sources[key] === undefined) {
	// 						const url = await this.getSignedUrl(bucket, key);
	// 						sources[key] = url;
	// 					}
	// 					return;
	// 				});
	// 				this.setState({
	// 					sources: { ...prevState.sources, ...sources },
	// 					loading: false,
	// 					newPet: false,
	// 				});
	// 			}
	// 		});
	// 	}
	// }
	async getSignedUrl(bucket, key) {
		let credentials = await Auth.currentCredentials();
		credentials = Auth.essentialCredentials(credentials);
		const s3 = new S3({
			apiVersion: '2006-03-01',
			credentials,
		});
		const params = { Bucket: bucket, Key: key };
		return new Promise((resolve, reject) => {
			s3.getSignedUrl('getObject', params, (error, url) => {
				if (error) {
					console.log('getSignedUrl error -', error);
					reject(error);
					return;
				}
				console.log('getSignedUrl url -', url);
				resolve(url);
			});
		});
	}
	showActionSheet = () => {
		this.actionSheet.show();
	};
	sendByEmail() {
		this.props.navigation.navigate('Contacts', { sendBy: 'email' });
	}
	sendBySMS() {
		this.props.navigation.navigate('Contacts', { sendBy: 'sms' });
	}
	async shareLink() {
		const referralLink = 'https://kaniro.uk/'; // await getReferralLink();
		const content = {
			title: '',
			message: `Hey,\n\nKaniro is the application that lets you keep track of your pets\' health.\n\nGet it for free at ${referralLink}`,
		};
		const options = { subject: 'Kaniro app' };
		const { action, activityType } = await Share.share(content, options);
		console.log('share link -', action, activityType);
	}
	onInviteFriends = (buttonIndex) => {
		switch (buttonIndex) {
		case 1:
			this.sendByEmail();
			break;
		case 2:
			this.sendBySMS();
			break;
		case 3:
			this.shareLink();
			break;
		default:
			break;
		}
	};
	renderItem = (item) => {
		const { sources } = this.state;
		return (
			<View style={[styles.itemContainer, { backgroundColor: item.color }]}>
				{sources && (
					<Image
						source={{ uri: sources[item.image.key] }}
						style={{
							width: 100,
							height: 100,
							marginBottom: 5,
							alignSelf: 'center',
						}}
					/>
				)}
				<Text style={styles.text}>{`Name: ${item.name}`}</Text>
				<Text style={styles.text}>{`Breed: ${item.breed}`}</Text>
				<Text style={styles.text}>{`Gender: ${item.gender}`}</Text>
				<Text style={styles.text}>{`Birth date: ${moment(
					Number(item.birthDate)
				).format('MMM YYYY D')}`}</Text>
				<Text style={styles.text}>{`Adoption date: ${moment(
					Number(item.adoptedDate)
				).format('MMM YYYY D')}`}</Text>
			</View>
		);
	};
	render() {
		// const { loading } = this.state;
		// const { pets } = this.props.user;
		// if (loading) {
		// 	return <Loader />;
		// }
		return (
			<View style={styles.container}>
				{/* <GridView
					style={styles.gridView}
					itemDimension={130}
					items={pets.map((pet, index) => ({
						...pet,
						color: colorPalette[index],
					}))}
					renderItem={this.renderItem}
				/> */}
				<View style={styles.buttonsContainer}>
					{/* <Button
						title="Add pet"
						onPress={() =>
							this.props.navigation.navigate('AddPet', { from: 'home' })
						}
						containerStyle={styles.buttonContainer}
					/> */}
					<Button
						title="Invite friends"
						onPress={this.showActionSheet}
						containerStyle={styles.buttonContainer}
					/>
					{/* <Button
						title="Restart"
						onPress={() => RNRestart.Restart()}
						containerStyle={styles.buttonContainer}
					/> */}
				</View>
				<ActionSheet
					ref={(actionSheet) => (this.actionSheet = actionSheet)}
					options={['Cancel', 'Mail', 'Message', 'More']}
					cancelButtonIndex={0}
					destructiveButtonIndex={1}
					onPress={this.onInviteFriends}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	gridView: {
		paddingTop: 25,
		flex: 1,
	},
	itemContainer: {
		justifyContent: 'flex-end',
		borderRadius: 5,
		padding: 10,
		height: 240,
	},
	text: {
		fontWeight: '600',
		fontSize: 14,
		color: '#fff',
	},
	buttonsContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	buttonContainer: {
		margin: 20,
	},
	modal: {
		margin: 0,
	},
});
export default Info;
