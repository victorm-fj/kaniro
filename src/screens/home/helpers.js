import { Alert } from 'react-native';
import branch from 'react-native-branch';
import Contacts from 'react-native-contacts';

export const getReferralLink = async () => {
	const branchUniversalObject = await branch.createBranchUniversalObject(
		'canonicalIdentifier',
		{
			automaticallyListOnSpotlight: true,
			metadata: { prop1: 'test', prop2: 'abc' },
			title: 'Cool pet\'s app!',
			contentDescription: 'Hey register your pets and manage their data.',
		}
	);
	const linkProperties = {
		feature: 'share',
		channel: 'InAppSMS',
	};
	const controlParams = {
		$desktop_url: 'http://desktop-url.com/monster/12345',
	};
	const { url } = await branchUniversalObject.generateShortUrl(
		linkProperties,
		controlParams
	);
	return url;
};

export const shareReferralLink = async () => {
	const branchUniversalObject = await branch.createBranchUniversalObject(
		'canonicalIdentifier',
		{
			automaticallyListOnSpotlight: true,
			metadata: { prop1: 'test', prop2: 'abc' },
			title: 'Cool pet\'s app!',
			contentDescription: 'Hey register your pets and manage their data.',
		}
	);
	const linkProperties = {
		feature: 'share',
		channel: 'InAppSMS',
	};
	const controlParams = {
		$desktop_url: 'http://desktop-url.com/monster/12345',
	};
	const shareOptions = {
		messageHeader: 'Check this out',
		messageBody: 'No really, check this out!',
	};
	const {
		channel,
		completed,
		error,
	} = await branchUniversalObject.showShareSheet(
		shareOptions,
		linkProperties,
		controlParams
	);
	console.log(channel, completed, error);
};

export const getContactsList = () =>
	new Promise((resolve, reject) => {
		Contacts.getAll((error, contacts) => {
			if (error && error === 'denied') {
				console.log('getContactsList error -', error);
				Alert.alert(
					'Can\'t access your contacts list',
					'Please go to Settings > Privacy > Contacts, to allow \'Kaniro\' to access your Contacts',
					[{ text: 'OK', onPress: () => {} }]
				);
				reject(error);
				return;
			}
			resolve(contacts);
		});
	});
