import { graphql } from 'react-apollo';
import Contacts from './Contacts';
import { InviteFriendsMutation } from '../../../graphQL';
import requirePermissions from '../../../components/hocs/requirePermissions';

export default graphql(InviteFriendsMutation, {
	options: {
		fetchPolicy: 'no-cache', // Error: fetchPolicy for mutations currently only supports the 'no-cache' policy
	},
	props: ({ ownProps, mutate }) => ({
		onInviteFriends: (data) =>
			mutate({
				variables: data,
				optimisticResponse: {
					__typename: 'Mutation',
					inviteFriends: data.friends.map((friend) => ({
						__typename: 'FriendType',
						phoneNumber: '',
						email: '',
						...friend,
					})),
				},
			}),
	}),
})(requirePermissions(Contacts, ['contacts']));
