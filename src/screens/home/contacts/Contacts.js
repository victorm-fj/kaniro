import React from 'react';
import { StyleSheet, View, Alert, Platform, Text } from 'react-native';
import { ListItem, Button, SearchBar, Icon } from 'react-native-elements';
import CountryPicker from 'react-native-country-picker-modal';
import CarrierInfo from 'react-native-carrier-info';
import { getContactsList, getReferralLink } from '../helpers';
import Loader from '../../../components/Loader';
import SearchableFlatList from '../../../components/SearchableFlatList';
import { getCountry } from '../../../utils/countriesInfo';

const letters = [
	'a',
	'b',
	'c',
	'd',
	'e',
	'f',
	'g',
	'h',
	'i',
	'j',
	'k',
	'l',
	'm',
	'n',
	'o',
	'p',
	'q',
	'r',
	's',
	't',
	'u',
	'v',
	'w',
	'x',
	'y',
	'z',
	'#',
];
class Contacts extends React.Component {
	state = {
		invites: [],
		contacts: [],
		userCountry: '',
		userCallingCode: '',
		loading: true,
		searchTerm: '',
	};
	async componentDidMount() {
		let userCountry = '';
		try {
			userCountry = await CarrierInfo.isoCountryCode();
		} catch (error) {
			console.log('isoCountryCode error', error);
			this.setState({ userCountry: 'US', userCallingCode: '1' });
		} finally {
			userCountry = userCountry === '' ? 'US' : userCountry;
			const userCallingCode = getCountry(userCountry).dial;
			console.log(
				'userCountry',
				userCountry,
				'userCallingCode',
				userCallingCode
			);
			this.setState(
				{
					userCountry: userCountry.toUpperCase(),
					userCallingCode,
				},
				() => this.getContactsList()
			);
		}
	}
	async getContactsList() {
		let contactsList = [];
		try {
			contactsList = await getContactsList();
		} catch (error) {
			console.log('getting contacts error', error);
			this.displayErrorAlert();
		} finally {
			if (contactsList.length > 0) {
				const contacts = contactsList.map((contact) => ({
					...contact,
					callingCode: this.state.userCallingCode,
					country: this.state.userCountry,
					searchStr: `${contact.givenName} ${contact.familyName}`,
				}));
				this.setState({ contacts, loading: false });
				// setTimeout(() => this.setState({ loading: false }), 1500);
			}
		}
	}
	displayErrorAlert() {
		Alert.alert('Error', 'Something went wrong, can not get contacts list', [
			{
				text: 'OK',
				onPress: () => console.log('OK Pressed'),
			},
		]);
	}
	selectCountry = (country, recordID) => {
		console.log('selectCountry', country, 'recordID', recordID);
		this.setState((prevState) => {
			const updatedContacts = prevState.contacts.map((contact) => {
				if (contact.recordID === recordID) {
					return {
						...contact,
						callingCode: country.callingCode,
						country: country.cca2,
					};
				}
				return contact;
			});
			return { contacts: [...updatedContacts] };
		});
	};
	onPressCheckBox = (recordID) => {
		console.log('recordID', recordID);
		this.setState((prevState) => {
			const recordIDIndex = prevState.invites.indexOf(recordID);
			if (recordIDIndex < 0) {
				return { invites: [...prevState.invites, recordID] };
			}
			const filteredInvites = prevState.invites.filter(
				(invite) => invite !== recordID
			);
			return { invites: [...filteredInvites] };
		});
	};
	onPressinviteContacts = async () => {
		// const { sendBy } = this.props.navigation.state.params;
		// const { contacts, invites } = this.state;
		// const downloadURL = await getReferralLink();
		// const friends = contacts
		// 	.filter((contact) => invites.indexOf(contact.recordID) >= 0)
		// 	.map(({ recordID, phoneNumbers, callingCode, emailAddresses }) => {
		// 		const phone = phoneNumbers[0].number;
		// 		if (sendBy === 'sms') {
		// 			let phoneNumber = '';
		// 			if (/^\+/.test(phone)) {
		// 				phoneNumber = `+${phone.replace(/\D/g, '')}`;
		// 			} else {
		// 				phoneNumber = `+${callingCode}${phone.replace(/\D/g, '')}`;
		// 			}
		// 			return {
		// 				recordId: recordID,
		// 				phoneNumber,
		// 			};
		// 		}
		// 		return {
		// 			recordId: recordID,
		// 			email: emailAddresses[0].email,
		// 		};
		// 	});
		// const data = { friends, downloadURL, sendBy };
		// console.log('data', data);
		try {
			// await this.props.onInviteFriends(data);
			this.props.navigation.goBack();
		} catch (error) {
			console.log('something went wrong', error);
			Alert.alert('Error', 'Something went wrong, please try again!', [
				{ text: 'OK', onPress: () => {} },
			]);
		}
	};
	onSearch = (searchTerm) => {
		this.setState({ searchTerm: searchTerm.trim() });
	};
	onCancelSearch = () => {
		this.setState({ searchTerm: '' });
		// this.searchBar.clear();
	};
	renderItem = ({ item }) => {
		const { sendBy } = this.props.navigation.state.params;
		const {
			recordID,
			givenName,
			middleName,
			familyName,
			phoneNumbers,
			country,
			emailAddresses,
		} = item;
		const checked = this.state.invites.indexOf(recordID) < 0 ? false : true;
		const phoneNumber = phoneNumbers.length > 0 ? phoneNumbers[0].number : '';
		const emailAddress =
			emailAddresses.length > 0 ? emailAddresses[0].email : '';
		const phoneLabel = phoneNumbers.length > 0 ? phoneNumbers[0].label : '';
		const emailLabel = emailAddresses.length > 0 ? emailAddresses[0].label : '';
		const subtitle = sendBy === 'sms' ? phoneNumber : emailAddress;
		const checkBoxTitle = sendBy === 'sms' ? phoneLabel : emailLabel;
		const title = `${givenName || ''} ${middleName || ''} ${familyName || ''}`;
		const disabled = subtitle === '';
		const leftIcon =
			sendBy === 'sms' ? (
				<CountryPicker
					ref={(countryPicker) => (this.countryPicker = countryPicker)}
					onChange={(country) => this.selectCountry(country, recordID)}
					cca2={country}
					onClose={() => {}}
				/>
			) : null;
		const rightIcon = (
			<View style={{ flexDirection: 'row', alignItems: 'center' }}>
				<Text style={{ color: '#8e8e93', fontWeight: 'bold' }}>
					{checkBoxTitle}
				</Text>
				<Icon
					name={checked ? 'ios-checkmark-circle' : 'ios-radio-button-off'}
					type="ionicon"
					color={checked ? 'rgb(0,122,255)' : '#8e8e93'}
					onPress={disabled ? () => {} : () => this.onPressCheckBox(recordID)}
					size={36}
					containerStyle={{ marginLeft: 10 }}
				/>
			</View>
		);
		return (
			<ListItem
				key={recordID}
				title={title}
				titleStyle={{ fontWeight: 'bold' }}
				subtitle={subtitle}
				subtitleStyle={{ color: '#8e8e93' }}
				disabled={disabled}
				leftIcon={leftIcon}
				rightIcon={rightIcon}
				bottomDivider
			/>
		);
	};
	renderSectionHeader = ({ section: { title } }) => {
		return (
			<View style={styles.sectionHeader}>
				<Text style={styles.headerTitle}>{title}</Text>
			</View>
		);
	};
	getSections() {
		const { contacts } = this.state;
		const sectionsObj = {};
		letters.forEach((letter) => {
			const title = letter.toUpperCase();
			sectionsObj[title] = { title, data: [] };
		});
		contacts.forEach((contact) => {
			const firstLetter = contact.givenName.slice(0, 1).toUpperCase();
			if (firstLetter.match(/[a-z]/i)) {
				sectionsObj[firstLetter].data.push(contact);
			} else {
				sectionsObj['#'].data.push(contact);
			}
		});
		return Object.values(sectionsObj).filter(
			(section) => section.data.length > 0
		);
	}
	render() {
		console.log(this.state.contacts);
		const { loading, searchTerm, invites } = this.state;
		if (loading) {
			return <Loader />;
		}
		return (
			<View style={styles.container}>
				<SearchBar
					ref={(searchBar) => (this.searchBar = searchBar)}
					platform={Platform.OS}
					placeholder="Search"
					onChangeText={this.onSearch}
					onCancel={this.onCancelSearch}
					onClear={this.onCancelSearch}
					containerStyle={styles.containerSearchBar}
				/>
				<SearchableFlatList
					style={styles.container}
					searchProperty="searchStr"
					searchTerm={searchTerm}
					renderItem={this.renderItem}
					renderSectionHeader={this.renderSectionHeader}
					keyExtractor={(item, index) => String(index)}
					sections={this.getSections()}
				/>
				<View style={styles.footer}>
					{invites.length > 0 && (
						<Button
							title={`Send ${invites.length} Invite${
								invites.length > 1 ? 's' : ''
							}`}
							titleStyle={styles.titleStyle}
							onPress={this.onPressinviteContacts}
							containerStyle={styles.buttonContainer}
							clear
						/>
					)}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	text: {
		fontSize: 18,
	},
	sectionHeader: {
		height: 32,
		paddingLeft: 10,
		backgroundColor: 'rgb(237,240,245)',
		justifyContent: 'center',
	},
	headerTitle: {
		fontSize: 16,
		fontWeight: 'bold',
	},
	footer: {
		height: 44,
		borderTopWidth: StyleSheet.hairlineWidth,
		borderTopColor: '#d8d8d8',
		backgroundColor: 'rgb(239,239,244)',
		justifyContent: 'center',
	},
	titleStyle: {
		color: 'rgb(0,122,255)',
		fontWeight: 'bold',
	},
	buttonContainer: {
		alignSelf: 'flex-end',
		marginRight: 5,
	},
	containerSearchBar: {
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: '#d8d8d8',
	},
});
export default Contacts;
