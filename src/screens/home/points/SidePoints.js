// @flow
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { human, systemWeights, iOSColors } from 'react-native-typography';

type Props = {
	points: number,
	text: string,
};
const SidePoints = (props: Props) => {
	return (
		<View style={styles.container}>
			<Text style={[human.title2, systemWeights.semibold]}>{props.points}</Text>
			<Text style={[human.footnote, { color: iOSColors.gray }]}>
				{props.text}
			</Text>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 20,
	},
});
export default SidePoints;
