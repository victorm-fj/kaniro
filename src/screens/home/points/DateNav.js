import React from 'react';
import { View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { human, iOSColors, systemWeights } from 'react-native-typography';

const DateNav = () => {
	return (
		<View style={{ flexDirection: 'row', alignItems: 'center' }}>
			<Icon
				type="ionicon"
				name="ios-arrow-dropleft"
				size={35}
				color={iOSColors.gray}
				onPress={() => {}}
			/>
			<View style={{ flex: 1 }}>
				<Text
					style={[human.body, systemWeights.light, { textAlign: 'center' }]}
				>
					TODAY
				</Text>
			</View>
			<Icon
				type="ionicon"
				name="ios-arrow-dropright"
				size={35}
				color={iOSColors.gray}
				onPress={() => {}}
			/>
		</View>
	);
};

export default DateNav;
