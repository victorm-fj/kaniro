import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import { human, iOSColors, systemWeights } from 'react-native-typography';

const EarnedPoints = (props) => {
	return (
		<View style={styles.container}>
			<Text style={[human.largeTitle, systemWeights.semibold]}>100</Text>
			<Text style={[human.footnote, { color: iOSColors.gray }]}>
				Points earned
			</Text>
			<Button
				title="DETAIL"
				titleStyle={[human.caption2, { color: iOSColors.gray, padding: 5 }]}
				clear
				containerStyle={styles.detailButton}
				buttonStyle={{ padding: 0 }}
				onPress={() => props.navigation.navigate('DetailNav')}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	detailButton: {
		borderRadius: 20,
		borderColor: iOSColors.gray,
		borderWidth: StyleSheet.hairlineWidth,
		padding: 0,
		margin: 0,
		position: 'absolute',
		bottom: -10,
	},
});
export default withNavigation(EarnedPoints);
