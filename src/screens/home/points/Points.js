import React from 'react';
import { StyleSheet } from 'react-native';
import { iOSColors } from 'react-native-typography';
import { AnimatedCircularProgress } from 'react-native-circular-progress';

import Card from '../../../components/card';
import DateNav from './DateNav';
import SidePoints from './SidePoints';
import EarnedPoints from './EarnedPoints';

const Points = () => {
	return (
		<Card>
			<Card.Header>
				<DateNav />
			</Card.Header>
			<Card.Content style={styles.cardContent}>
				<SidePoints points={2500} text="Potential" />
				<AnimatedCircularProgress
					size={150}
					width={8}
					fill={70}
					tintColor={iOSColors.blue}
					onAnimationComplete={() => console.log('onAnimationComplete')}
					backgroundColor={iOSColors.lightGray}
					arcSweepAngle={300}
					rotation={-150}
					linecap="round"
				>
					{() => <EarnedPoints />}
				</AnimatedCircularProgress>
				<SidePoints points={2400} text="Remaining" />
			</Card.Content>
		</Card>
	);
};

const styles = StyleSheet.create({
	cardContent: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 0,
		paddingBottom: 60,
	},
});

export default Points;
