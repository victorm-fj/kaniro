import React from 'react';

import Card from '../../../components/card';
import AddWater from '../components/AddWater';

const total = {
	totalQuantity: 0.4,
	measure: 'liters',
	remainingDays: 0,
	recommendedQuantity: 2,
};

const Water = () => {
	return (
		<Card>
			<Card.Header divider title="WATER" />
			<Card.Content>
				<AddWater />
			</Card.Content>
			<Card.Footer divider total={total} />
		</Card>
	);
};

export default Water;
