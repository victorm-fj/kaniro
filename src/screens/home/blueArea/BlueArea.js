import React from 'react';
import { View } from 'react-native';
import Line from '../components/Line';
import { iOSColors } from 'react-native-typography';

const BlueArea = () => {
	return (
		<View
			style={{
				width: '115%',
				height: 150,
				transform: [{ rotate: '180deg' }],
				position: 'absolute',
			}}
		>
			<Line
				values={[4, 8, 10, 10, 10, 10]}
				fillColor={iOSColors.blue}
				strokeColor={iOSColors.blue}
			/>
		</View>
	);
};

export default BlueArea;
