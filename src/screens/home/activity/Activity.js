import React from 'react';
import { FlatList } from 'react-native';
import { iOSColors } from 'react-native-typography';

import Card from '../../../components/card';
import ActivityItem from '../components/ActivityItem';
import SwipeOut from '../../../components/swipeOut/SwipeOut';

const rightButtons = [
	{
		title: 'Active',
		titleColor: iOSColors.white,
		iconName: 'ios-checkmark-circle',
		iconSize: 30,
		backgroundColor: iOSColors.green,
	},
	{
		title: 'Delete',
		titleColor: iOSColors.white,
		iconName: 'ios-trash',
		iconSize: 30,
		backgroundColor: iOSColors.blue,
	},
];

const data = [
	{
		id: 0,
		date: 'Today, 17:30 PM',
		activity: 'Walk Axo in the park',
		duration: '60 mins',
		status: 'On',
	},
	{
		id: 1,
		date: 'Today, 17:30 PM',
		activity: 'Walk Axo in the park',
		duration: '60 mins',
		status: 'On',
	},
	{
		id: 2,
		date: 'Today, 17:30 PM',
		activity: 'Walk Axo in the park',
		duration: '60 mins',
		status: 'On',
	},
];
class Activiy extends React.Component {
	renderItem = ({ item }) => {
		return (
			<SwipeOut rightButtons={rightButtons}>
				<Card.Content divider style={{ backgroundColor: iOSColors.white }}>
					<ActivityItem activityItem={item} />
				</Card.Content>
			</SwipeOut>
		);
	};
	render() {
		return (
			<Card style={{ paddingBottom: 8 }}>
				<Card.Header
					title="ACTIVITY PLAN"
					buttonTitle="ADD PLAN"
					divider
					icon
				/>
				<FlatList
					data={data}
					extraData={data}
					keyExtractor={(item, index) => String(index)}
					renderItem={this.renderItem}
				/>
			</Card>
		);
	}
}

export default Activiy;
