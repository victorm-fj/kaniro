import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { human, iOSColors } from 'react-native-typography';

const Deal = (props) => {
	return (
		<TouchableOpacity onPress={props.redeemDeal} style={styles.deal}>
			<Text style={human.subheadWhite}>{props.deal.subtitle}</Text>
			<Text style={human.subheadWhite}>{props.deal.title.toUpperCase()}</Text>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	deal: {
		height: 50,
		width: '100%',
		backgroundColor: iOSColors.blue,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		padding: 10,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: 'rgba(0, 0, 0, .3)',
	},
});
export default Deal;
