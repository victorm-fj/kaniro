import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';

import Deal from './Deal';
import Goal from './goal/Goal';
import Points from './points/Points';
import Greeting from './Greeting';
import ClaimPoints from './components/ClaimPoints';
import AuthButtons from './components/AuthButtons';
import Food from './food/Food';
import Water from './water/Water';
import Health from './health/Health';
import Activity from './activity/Activity';
import UserGoal from './goal/UserGoal';
import BlueArea from './blueArea/BlueArea';

class Home extends React.Component {
	state = { isDealActive: true, signIn: false };
	redeemDeal = () => {
		this.setState({ isDealActive: false });
	};
	onSignIn = () => {
		this.setState({ signIn: true });
	};
	toggleSignIn = () => {
		this.setState((prevState) => ({ signIn: !prevState.signIn }));
	};
	render() {
		const { deal } = this.props.screenProps;
		const { isDealActive, signIn } = this.state;
		return (
			<View style={styles.container}>
				{deal &&
					isDealActive && <Deal deal={deal} redeemDeal={this.redeemDeal} />}
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<BlueArea />
					<Greeting />
					<Goal />
					<UserGoal />
					<Points />
					{!signIn && <ClaimPoints onSignIn={this.onSignIn} />}
					{signIn && <AuthButtons toggleSignIn={this.toggleSignIn} />}
					<Food />
					<Activity />
					<Water />
					<Health />
				</ScrollView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	scrollContainer: {
		flexGrow: 1,
		alignItems: 'center',
		paddingBottom: 40,
	},
});
export default Home;
