import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { iOSColors, human } from 'react-native-typography';

import InfoItem from './InfoItem';

const ActivityItem = (props) => {
	const {
		activityItem: { date, activity, duration, status, type = 'activity' },
	} = props;
	return (
		<View>
			<InfoItem
				iconName="ios-radio-button-on"
				iconSize={14}
				iconColor={type === 'health' ? iOSColors.green : iOSColors.red}
				info={date}
			/>
			<Text style={[human.title3, styles.text]}>{activity}</Text>
			<View style={styles.row}>
				<InfoItem iconName="ios-alarm" info={duration} />
				<InfoItem iconName="ios-alarm" info={status} />
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 10,
	},
});
export default ActivityItem;
