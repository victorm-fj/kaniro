import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import { human, systemWeights, iOSColors } from 'react-native-typography';

const ClaimPoints = (props) => {
	return (
		<View>
			<Button
				title="CLAIM POINTS"
				titleStyle={[human.subheadWhite, systemWeights.bold]}
				clear
				containerStyle={[styles.claimBtnContainer, styles.boxShadow]}
				onPress={props.onSignIn}
			/>
			<View style={styles.signInContainer}>
				<Text
					style={[human.footnote, { color: iOSColors.gray, paddingRight: 10 }]}
				>
					Got an account?
				</Text>
				<Button
					title="Sign In"
					titleStyle={[human.footnote, styles.signInText]}
					clear
					onPress={props.onSignIn}
				/>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	claimBtnContainer: {
		backgroundColor: iOSColors.blue,
		borderRadius: 10,
		paddingVertical: 5,
		paddingHorizontal: 15,
		marginTop: -30,
	},
	boxShadow: {
		...Platform.select({
			ios: {
				shadowColor: 'rgba(0,0,0,0.1)',
				shadowOpacity: 1,
				shadowRadius: 5, //StyleSheet.hairlineWidth,
				shadowOffset: {
					width: 0,
					height: 2, //StyleSheet.hairlineWidth,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
	signInContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		marginVertical: 20,
	},
	signInText: {
		padding: 0,
		fontWeight: 'bold',
		color: iOSColors.black,
	},
});
export default ClaimPoints;
