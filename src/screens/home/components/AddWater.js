import React from 'react';
import { StyleSheet, View, Platform, ScrollView } from 'react-native';
import { iOSColors } from 'react-native-typography';
import { Icon } from 'react-native-elements';

const iconPrefix = Platform.OS === 'ios' ? 'ios' : 'md';
const waterData = [
	{ id: 0, full: true },
	{ id: 1, full: true },
	{ id: 2, full: false },
	{ id: 3, full: false },
	{ id: 4, full: false },
	{ id: 5, full: false },
	{ id: 6, full: false },
];

class AddWater extends React.Component {
	state = { glasses: waterData, glassesNumber: 2 };
	toggleWater = (id) => {
		this.setState((prevState) => {
			if (
				id === prevState.glassesNumber ||
				id === prevState.glassesNumber - 1
			) {
				let glassesNumber = 0;
				const glasses = prevState.glasses.map((glass) => {
					return glass.id === id ? { ...glass, full: !glass.full } : glass;
				});
				glasses.forEach((glass) => {
					if (glass.full) {
						glassesNumber++;
					}
				});
				return {
					glasses,
					glassesNumber,
				};
			}
			return null;
		});
	};
	render() {
		return (
			<ScrollView horizontal contentContainerStyle={[styles.row]}>
				{this.state.glasses.map((data) => {
					return (
						<View
							key={data.id}
							style={{ justifyContent: 'center', alignItems: 'center' }}
						>
							{data.id === this.state.glassesNumber && (
								<Icon
									name={`${iconPrefix}-add`}
									type="ionicon"
									color={iOSColors.red}
									size={30}
									containerStyle={{ position: 'absolute' }}
									iconStyle={{ fontWeight: 'bold' }}
								/>
							)}
							<Icon
								name={`ios-water${data.full ? '' : '-outline'}`}
								type="ionicon"
								color={iOSColors.blue}
								size={50}
								containerStyle={{ marginHorizontal: 10 }}
								onPress={() => this.toggleWater(data.id)}
							/>
						</View>
					);
				})}
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
});
export default AddWater;
