import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { Button } from 'react-native-elements';
import { human, systemWeights, iOSColors } from 'react-native-typography';

const AuthButton = (props) => {
	return (
		<View style={styles.container}>
			<Button
				title="SIGN IN"
				titleStyle={[human.subhead, systemWeights.bold]}
				clear
				containerStyle={[styles.singInContainer, styles.boxShadow]}
				onPress={props.toggleSignIn}
			/>
			<Button
				title="SIGN UP"
				titleStyle={[human.subheadWhite, systemWeights.bold]}
				clear
				containerStyle={[styles.signUpContainer, styles.boxShadow]}
				onPress={props.toggleSignIn}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		width: '90%',
		flexDirection: 'row',
		justifyContent: 'space-around',
		marginVertical: 20,
	},
	singInContainer: {
		backgroundColor: iOSColors.white,
		borderRadius: 30,
		paddingVertical: 5,
		paddingHorizontal: 15,
		width: '45%',
	},
	signUpContainer: {
		backgroundColor: iOSColors.blue,
		borderRadius: 30,
		paddingVertical: 5,
		paddingHorizontal: 15,
		width: '45%',
	},
	boxShadow: {
		...Platform.select({
			ios: {
				shadowColor: 'rgba(0,0,0,0.1)',
				shadowOpacity: 1,
				shadowRadius: 5, //StyleSheet.hairlineWidth,
				shadowOffset: {
					width: 0,
					height: 2, //StyleSheet.hairlineWidth,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
});
export default AuthButton;
