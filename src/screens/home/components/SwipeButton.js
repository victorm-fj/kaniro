// @flow
import React from 'react';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { human, iOSColors } from 'react-native-typography';

type Props = {
	onPress: Function,
	iconName: string,
	iconSize: number,
	title: string,
};
const SwipeButton = (props: Props) => {
	const { onPress, iconName, iconSize, title } = props;
	return (
		<View style={styles.container}>
			<TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
				<Ionicons name={iconName} color={iOSColors.white} size={iconSize} />
				<Text style={[human.bodyWhite]}>{title}</Text>
			</TouchableOpacity>
		</View>
	);
};
SwipeButton.defaultProps = {
	onPress: () => {},
	iconSize: 24,
	iconName: 'ios-trash',
	title: 'Delete',
};

const styles = StyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center',
		width: 75,
		height: 110,
		backgroundColor: iOSColors.blue,
	},
	buttonContainer: {
		justifyContent: 'center',
		alignItems: 'center',
	},
});
export default SwipeButton;
