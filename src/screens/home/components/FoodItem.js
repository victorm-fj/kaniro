// @flow
import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { human, iOSColors } from 'react-native-typography';

import InfoItem from './InfoItem';

type Props = {
	foodItem: {
		id: number,
		title: string,
		calPercentage: number,
		foodQuantity: string,
		completed: boolean,
	},
};
const FoodItem = (props: Props) => {
	const {
		foodItem: { title, calPercentage, foodQuantity },
	} = props;
	return (
		<View style={styles.item}>
			<View style={styles.actions}>
				<Icon
					type="ionicon"
					name="ios-radio-button-on"
					color={iOSColors.purple}
					size={35}
				/>
				<Icon
					type="ionicon"
					name="ios-checkmark-circle"
					color={iOSColors.green}
					size={20}
				/>
			</View>
			<View style={styles.info}>
				<Text style={[human.body, styles.text]}>{title}</Text>
				<View style={styles.row}>
					<InfoItem info={`${calPercentage}% cal`} iconName="ios-flame" />
					<InfoItem info={foodQuantity} iconName="ios-nutrition" />
				</View>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	item: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
	actions: {
		alignItems: 'center',
	},
	info: {
		flex: 1,
		paddingLeft: 15,
	},
	row: {
		flexDirection: 'row',
	},
	text: {
		paddingVertical: 5,
	},
});
export default FoodItem;
