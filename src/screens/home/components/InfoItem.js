// @flow
import React from 'react';
import { StyleSheet, View, Text, ViewPropTypes } from 'react-native';
import { human, iOSColors } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

type Props = {
	iconName: string,
	info: string,
	iconSize: number,
	iconColor: string,
	style: ViewPropTypes.style,
};
const InfoItem = (props: Props) => {
	return (
		<View style={[styles.container, props.style]}>
			<Ionicons
				name={props.iconName}
				size={props.iconSize}
				color={props.iconColor}
			/>
			<Text style={[human.subhead, styles.info]}>{props.info}</Text>
		</View>
	);
};
InfoItem.defaultProps = {
	iconName: 'ios-flame',
	info: 'Some information',
	iconSize: 18,
	iconColor: iOSColors.gray,
	style: {},
};

const styles = StyleSheet.create({
	container: {
		flexDirection: 'row',
		paddingRight: 15,
		alignItems: 'center',
	},
	info: {
		color: iOSColors.gray,
		paddingLeft: 5,
	},
});
export default InfoItem;
