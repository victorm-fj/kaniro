import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { iOSColors } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Card from '../../../components/card';
import { GraySubheading, MainTitle } from '../../../components/text';

const Goal = () => {
	return (
		<Card>
			<Card.Header style={{ paddingBottom: 0 }}>
				<GraySubheading>Your goal</GraySubheading>
			</Card.Header>
			<Card.Content style={styles.content}>
				<TouchableOpacity onPress={() => {}} style={styles.action}>
					<View style={styles.legend}>
						<Ionicons
							name="ios-locate-outline"
							size={40}
							color={iOSColors.blue}
						/>
						<MainTitle style={{ marginLeft: 10 }}>What's your goal?</MainTitle>
					</View>
					<Ionicons
						name="ios-arrow-forward"
						size={30}
						color={iOSColors.black}
					/>
				</TouchableOpacity>
			</Card.Content>
		</Card>
	);
};

const styles = StyleSheet.create({
	content: {
		flexDirection: 'row',
		alignItems: 'center',
		paddingTop: 5,
	},
	action: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	legend: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default Goal;
