import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
	VictoryArea,
	// VictoryScatter,
} from 'victory-native';
import { iOSColors } from 'react-native-typography';

import { GraySubheading, MainTitle } from '../../../components/text';
import Card from '../../../components/card';

const data = [
	{ x: 1, y: 2 },
	{ x: 2, y: 3 },
	{ x: 3, y: 5 },
	{ x: 4, y: 4 },
	{ x: 5, y: 7 },
];

const UserGoal = () => {
	return (
		<Card style={{ flexDirection: 'row' }}>
			<Card.Content separator style={[styles.content, { flex: 1 }]}>
				<GraySubheading>Your goal</GraySubheading>
				<MainTitle>Save money</MainTitle>
			</Card.Content>
			<Card.Content style={[styles.content, { flex: 1.25 }]}>
				<GraySubheading>Latest savings, Jan 22</GraySubheading>
				<View style={styles.status}>
					<MainTitle>27 £</MainTitle>
					<VictoryArea
						data={data}
						style={{
							data: {
								fill: iOSColors.lightGray,
								stroke: iOSColors.purple,
								strokeWidth: 3,
							},
						}}
						padding={{ top: 0, bottom: 0, left: 0, right: 0 }}
						width={50}
						height={35}
						interpolation="natural"
					/>
					{/* <VictoryScatter
							data={data.slice(-1)}
							size={5}
							style={{ data: { fill: iOSColors.purple } }}
							padding={{ top: 0, bottom: 0, left: 0, right: 0 }}
							width={50}
							height={35}
						/> */}
				</View>
			</Card.Content>
		</Card>
	);
};

const styles = StyleSheet.create({
	content: {
		justifyContent: 'center',
		paddingTop: 5,
	},
	status: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingRight: 20,
	},
	legend: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default UserGoal;
