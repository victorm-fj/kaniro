import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';
import Modal from 'react-native-modal';
import { human, iOSColors } from 'react-native-typography';
import { Divider, Icon, Button } from 'react-native-elements';

const iconPrefix = Platform.OS === 'ios' ? 'ios' : 'md';

const waterData = [
	{ id: 0, full: true },
	{ id: 1, full: true },
	{ id: 2, full: false },
	{ id: 3, full: false },
	{ id: 4, full: false },
	{ id: 5, full: false },
	{ id: 6, full: false },
];

class Water extends React.Component {
	state = { glasses: waterData, glassesNumber: 2 };
	toggleWater = (id) => {
		this.setState((prevState) => {
			if (
				id === prevState.glassesNumber ||
				id === prevState.glassesNumber - 1
			) {
				let glassesNumber = 0;
				const glasses = prevState.glasses.map((glass) => {
					return glass.id === id ? { ...glass, full: !glass.full } : glass;
				});
				glasses.forEach((glass) => {
					if (glass.full) {
						glassesNumber++;
					}
				});
				return {
					glasses,
					glassesNumber,
				};
			}
			return null;
		});
	};
	render() {
		return (
			<Modal
				isVisible={this.props.isVisible}
				hideModalContentWhileAnimating
				onSwipe={this.props.onSwipe}
				swipeDirection={this.props.swipeDirection}
				style={styles.modal}
			>
				<View style={{ flex: 1.25 }} />
				<View style={styles.container}>
					<View style={[{ flex: 0.25 }, styles.row]}>
						<Icon
							name="md-close"
							type="ionicon"
							color={iOSColors.blue}
							size={25}
							containerStyle={{
								position: 'absolute',
								top: 12,
								left: 20,
							}}
							onPress={this.props.onSwipe}
							iconStyle={{ fontWeight: 'bold' }}
							underlayColor="transparent"
						/>
						<Button
							title="Done"
							titleStyle={{ color: iOSColors.blue }}
							onPress={this.props.onSwipe}
							clear
							containerStyle={{
								position: 'absolute',
								top: 5,
								right: 15,
							}}
						/>
					</View>
					<Divider style={{ backgroundColor: iOSColors.lightGray }} />
					<View style={[{ flex: 1 }, styles.row]}>
						<Text style={human.headline}>WATER</Text>
						<Text style={human.body}>
							+ Register your first pet to continue
						</Text>
					</View>
					<Divider style={{ backgroundColor: iOSColors.lightGray }} />
					<View
						style={[
							styles.row,
							{
								flex: 2,
								justifyContent: 'center',
								alignItems: 'center',
								padding: 0,
							},
						]}
					>
						{this.state.glasses.map((data) => {
							return (
								<View
									key={data.id}
									style={{ justifyContent: 'center', alignItems: 'center' }}
								>
									{data.id === this.state.glassesNumber && (
										<Icon
											name={`${iconPrefix}-add`}
											type="ionicon"
											color={iOSColors.red}
											size={30}
											containerStyle={{ position: 'absolute' }}
											iconStyle={{ fontWeight: 'bold' }}
										/>
									)}
									<Icon
										name={`ios-water${data.full ? '' : '-outline'}`}
										type="ionicon"
										color={iOSColors.blue}
										size={50}
										containerStyle={{ marginHorizontal: 10 }}
										onPress={() => this.toggleWater(data.id)}
									/>
								</View>
							);
						})}
					</View>
					<Divider style={{ backgroundColor: iOSColors.lightGray }} />
					<View style={[{ flex: 2 }, styles.row]}>
						<Text style={human.title2}>0.4 liters</Text>
						<Text style={human.footnote}>Recommended 2-2.5 liters</Text>
					</View>
					<Divider
						style={{ backgroundColor: iOSColors.lightGray, marginBottom: 20 }}
					/>
				</View>
			</Modal>
		);
	}
}

const styles = StyleSheet.create({
	modal: {
		margin: 0,
		padding: 0,
	},
	container: {
		flex: 1,
		backgroundColor: iOSColors.white,
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 20,
	},
});
export default Water;
