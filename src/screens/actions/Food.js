import React from 'react';
import { StyleSheet, View, Text, Platform } from 'react-native';
import Modal from 'react-native-modal';
import { human, iOSColors, systemWeights } from 'react-native-typography';
import { Divider, Icon, Button } from 'react-native-elements';
import RNTooltips from 'react-native-tooltips';

const iconPrefix = Platform.OS === 'ios' ? 'ios' : 'md';

class Food extends React.Component {
	state = { visible: false, reference: undefined };
	render() {
		return (
			<Modal
				isVisible={this.props.isVisible}
				hideModalContentWhileAnimating
				onSwipe={this.props.onSwipe}
				swipeDirection={this.props.swipeDirection}
				style={styles.modal}
			>
				<View style={{ flex: 1.25 }} />
				<View style={styles.container}>
					<View style={[{ flex: 0.25 }, styles.row]}>
						<Icon
							name="md-close"
							type="ionicon"
							color={iOSColors.blue}
							size={25}
							containerStyle={{
								position: 'absolute',
								top: 12,
								left: 20,
							}}
							onPress={this.props.onSwipe}
							iconStyle={{ fontWeight: 'bold' }}
							underlayColor="transparent"
						/>
						<Button
							title="Done"
							titleStyle={{ color: iOSColors.blue }}
							onPress={this.props.onSwipe}
							clear
							containerStyle={{
								position: 'absolute',
								top: 5,
								right: 15,
							}}
						/>
					</View>
					<Divider style={{ backgroundColor: iOSColors.lightGray }} />
					<View style={[{ flex: 1 }, styles.row]}>
						<Text style={human.headline}>SELECT A MEAL</Text>
						<Text style={human.body}>
							+ Register your first pet to continue
						</Text>
					</View>
					<Divider style={{ backgroundColor: iOSColors.lightGray }} />
					<View
						style={[
							{
								flex: 3,
								justifyContent: 'center',
								alignItems: 'center',
								padding: 20,
							},
						]}
					>
						<Text style={[human.callout, { textAlign: 'center' }]}>
							Give Alfo 538g of his tailor-made blend, every day
						</Text>
						<Text style={[human.footnote, systemWeights.semibold]}>
							This will give Alfo 100% of his daily caloires
						</Text>
						<Icon
							name={`${iconPrefix}-pizza`}
							type="ionicon"
							color={iOSColors.orange}
							size={40}
						/>
						<Button
							title="Tap"
							titleStyle={{ color: iOSColors.blue }}
							clear
							ref={(pizza) => (this.pizza = pizza)}
							onPress={() =>
								this.setState((prevState) => ({
									visible: true,
									reference: this.pizza,
								}))
							}
						/>
						<RNTooltips
							text={'538g / 1947 kcal'}
							visible={this.state.visible}
							reference={this.state.reference}
							tintColor={iOSColors.green}
							textColor={iOSColors.white}
							autoHide
							duration={450}
							clickToHide
						/>
					</View>
					<Divider
						style={{ backgroundColor: iOSColors.lightGray, marginBottom: 20 }}
					/>
				</View>
			</Modal>
		);
	}
}

const styles = StyleSheet.create({
	modal: {
		margin: 0,
		padding: 0,
	},
	container: {
		flex: 1,
		backgroundColor: iOSColors.white,
	},
	row: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: 20,
	},
});
export default Food;
