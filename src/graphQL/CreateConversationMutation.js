import gql from 'graphql-tag';

export default gql`
	mutation CreateConversation(
		$appointmentId: ID!
		$conversationId: ID!
		$createdAt: String!
	) {
		createConversation(
			appointmentId: $appointmentId
			conversationId: $conversationId
			createdAt: $createdAt
		) {
			__typename
			appointmentId
			conversationId
			createdAt
		}
	}
`;
