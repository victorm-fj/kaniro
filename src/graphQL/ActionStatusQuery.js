import gql from 'graphql-tag';

export default gql`
	query ActionStatus {
		actionStatus @client {
			isActive
		}
	}
`;
