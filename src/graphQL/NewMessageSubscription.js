import gql from 'graphql-tag';

export default gql`
	subscription SubscribeToNewMessage($conversationId: ID!) {
		subscribeToNewMessage(conversationId: $conversationId) {
			__typename
			conversationId
			createdAt
			messageId
			senderId
			content
		}
	}
`;
