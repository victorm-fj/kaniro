import gql from 'graphql-tag';

export default gql`
	query MeAppointmentsQuery($userId: ID!) {
		me(userId: $userId) {
			__typename
			userId
			pets {
				__typename
				userId
				petId
				name
				breed
				gender
			}
			appointments {
				__typename
				appointmentId
				specialistId
				consultantType
				date
				mediaType
				description
				photos {
					__typename
					key
					bucket
					region
				}
				pet {
					__typename
					petId
					name
					image {
						__typename
						bucket
						region
						key
					}
				}
				conversation {
					__typename
					conversationId
				}
			}
		}
	}
`;
