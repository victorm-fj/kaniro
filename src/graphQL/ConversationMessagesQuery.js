import gql from 'graphql-tag';

export default gql`
	query ConversationMessages($conversationId: ID!) {
		conversationMessages(conversationId: $conversationId) {
			__typename
			nextToken
			messages {
				__typename
				conversationId
				createdAt
				messageId
				senderId
				content
			}
		}
	}
`;
