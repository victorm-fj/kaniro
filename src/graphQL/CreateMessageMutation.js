import gql from 'graphql-tag';

export default gql`
	mutation CreateMessage(
		$conversationId: ID!
		$createdAt: String!
		$messageId: ID!
		$senderId: ID!
		$content: String!
	) {
		createMessage(
			conversationId: $conversationId
			createdAt: $createdAt
			messageId: $messageId
			senderId: $senderId
			content: $content
		) {
			__typename
			conversationId
			createdAt
			messageId
			senderId
			content
		}
	}
`;
