import gql from 'graphql-tag';

export default gql`
	mutation addPet(
		$userId: ID!
		$petId: ID!
		$name: String!
		$breed: String!
		$gender: String!
		$birthDate: String
		$adoptedDate: String
		$image: ImageInput
	) {
		addPet(
			userId: $userId
			petId: $petId
			name: $name
			breed: $breed
			gender: $gender
			birthDate: $birthDate
			adoptedDate: $adoptedDate
			image: $image
		) {
			__typename
			userId
			petId
			name
			breed
			gender
			birthDate
			adoptedDate
			image {
				bucket
				region
				key
			}
		}
	}
`;
