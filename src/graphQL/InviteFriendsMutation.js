import gql from 'graphql-tag';

export default gql`
	mutation InviteFriends(
		$friends: [FriendInput!]!
		$downloadURL: String!
		$sendBy: String!
	) {
		inviteFriends(
			friends: $friends
			downloadURL: $downloadURL
			sendBy: $sendBy
		) {
			__typename
			recordId
			phoneNumber
			email
		}
	}
`;
