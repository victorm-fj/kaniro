import gql from 'graphql-tag';

export default gql`
	mutation BookAppointment(
		$userId: ID!
		$appointmentId: ID!
		$consultantType: String!
		$date: String!
		$mediaType: String!
		$description: String!
		$petId: ID!
		$specialistId: ID!
		$photos: [ImageInput!]
	) {
		bookAppointment(
			userId: $userId
			appointmentId: $appointmentId
			consultantType: $consultantType
			date: $date
			mediaType: $mediaType
			description: $description
			petId: $petId
			specialistId: $specialistId
			photos: $photos
		) {
			__typename
			userId
			appointmentId
			specialistId
			consultantType
			date
			mediaType
			description
			photos {
				__typename
				bucket
				key
				region
			}
			conversation {
				__typename
				conversationId
			}
			pet {
				__typename
				userId
				petId
				name
				breed
				gender
				image {
					__typename
					bucket
					key
					region
				}
			}
		}
	}
`;
