import gql from 'graphql-tag';

export default gql`
	query MeRoute($userId: ID!) {
		me(userId: $userId) {
			__typename
			userId
			routes {
				__typename
				nextToken
				routes {
					__typename
					userId
					routeId
					polyline
					startDate
					endDate
					steps
					distance
					score
				}
			}
		}
	}
`;
