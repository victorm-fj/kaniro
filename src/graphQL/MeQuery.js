import gql from 'graphql-tag';

export default gql`
	query Me {
		me {
			__typename
			userId
			registered
			pets {
				__typename
				userId
				petId
				name
				breed
				gender
				birthDate
				adoptedDate
				image {
					__typename
					bucket
					region
					key
				}
			}
		}
	}
`;
