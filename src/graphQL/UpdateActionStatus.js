import gql from 'graphql-tag';

export default gql`
	mutation UpdateActionStatus($isActive: Boolean) {
		updateActionStatus(isActive: $isActive) @client
	}
`;
