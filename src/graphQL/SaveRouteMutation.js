import gql from 'graphql-tag';

export default gql`
	mutation SaveRoute(
		$userId: ID!
		$routeId: ID!
		$polyline: String!
		$startDate: String!
		$endDate: String!
		$steps: String
		$distance: String
	) {
		saveRoute(
			userId: $userId
			routeId: $routeId
			polyline: $polyline
			startDate: $startDate
			endDate: $endDate
			steps: $steps
			distance: $distance
		) {
			__typename
			userId
			routeId
			polyline
			startDate
			endDate
			steps
			distance
			score
		}
	}
`;
