import gql from 'graphql-tag';

export default gql`
	mutation newUser($userId: ID!) {
		newUser(userId: $userId) {
			__typename
			userId
			registered
		}
	}
`;
