// @flow
import S3 from 'aws-sdk/clients/s3';
import { Auth } from 'aws-amplify';
import awsmobile from '../aws-exports';

export const uploadImage = async (
	key: string,
	mimeType: string,
	buffer: Buffer
) => {
	let credentials = await Auth.currentCredentials();
	credentials = Auth.essentialCredentials(credentials);
	const s3 = new S3({
		apiVersion: '2006-03-01',
		credentials,
	});
	const params = {
		Bucket: awsmobile.aws_user_files_s3_bucket,
		Key: key,
		ContentType: mimeType,
		Body: buffer,
	};
	return new Promise((resolve, reject) => {
		s3.putObject(params, (error, data) => {
			if (error) {
				console.log('putObject error -', error);
				reject(error);
				return;
			}
			console.log('putObject data -', data);
			resolve(data);
		});
	});
};
