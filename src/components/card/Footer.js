// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes, Text } from 'react-native';
import { iOSColors, human, systemWeights } from 'react-native-typography';
import { Icon } from 'react-native-elements';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	total: {
		totalQuantity: number,
		measure: string,
		remainingDays: number,
		recommendedQuantity: number,
	},
};
const Footer = (props: Props) => {
	const { style, divider, total, children } = props;
	return (
		<View
			style={[styles.container, { borderTopWidth: divider ? 1 : 0 }, style]}
		>
			{props.total && (
				<View style={styles.row}>
					<Text style={[human.body, systemWeights.bold]}>{`${
						total.totalQuantity
					} ${total.measure}`}</Text>
					<Icon
						type="ionicon"
						name="ios-information-circle-outline"
						containerStyle={{ marginHorizontal: 15 }}
						color={iOSColors.gray}
						size={24}
					/>
					{total.remainingDays && (
						<Text style={[human.subhead, { color: iOSColors.gray }]}>
							Remaining food for{' '}
							{`${total.remainingDays}-${total.remainingDays + 1}`} days
						</Text>
					)}
					{total.recommendedQuantity && (
						<Text style={[human.subhead, { color: iOSColors.gray }]}>
							Recommended{' '}
							{`${total.recommendedQuantity}-${total.recommendedQuantity +
								0.5}`}{' '}
							liters
						</Text>
					)}
				</View>
			)}
			{children}
		</View>
	);
};
Footer.defaultProps = {
	divider: false,
	total: null,
};

const styles = StyleSheet.create({
	container: {
		padding: 16,
		borderTopColor: iOSColors.lightGray,
		flexDirection: 'row',
		alignItems: 'center',
	},
	row: {
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default Footer;
