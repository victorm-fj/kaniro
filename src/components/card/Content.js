// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { iOSColors } from 'react-native-typography';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	separator: boolean,
};
const Content = (props: Props) => {
	return (
		<View
			style={[
				styles.container,
				{
					borderBottomWidth: props.divider ? 1 : 0,
					borderRightWidth: props.separator ? 1 : 0,
				},
				props.style,
			]}
		>
			{props.children}
		</View>
	);
};
Content.defaultProps = { divider: false, separator: false };

const styles = StyleSheet.create({
	container: {
		padding: 16,
		borderBottomColor: iOSColors.lightGray,
		borderRightColor: iOSColors.lightGray,
	},
});
export default Content;
