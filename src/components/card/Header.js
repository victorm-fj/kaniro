// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes, Text } from 'react-native';
import { human, iOSColors, systemWeights } from 'react-native-typography';
import { Button, Icon } from 'react-native-elements';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
	title: string,
	buttonTitle: string,
	icon: boolean,
};
const Header = (props: Props) => {
	const { style, divider, children, title, buttonTitle, icon } = props;
	return (
		<View
			style={[styles.container, { borderBottomWidth: divider ? 1 : 0 }, style]}
		>
			{title && (
				<View style={styles.title}>
					<Text style={[human.headline, systemWeights.bold]}>{title}</Text>
				</View>
			)}
			{buttonTitle && (
				<Button
					title={buttonTitle}
					titleStyle={[
						human.callout,
						{ color: iOSColors.blue, padding: 0, paddingLeft: 5 },
					]}
					icon={
						icon ? (
							<Icon
								type="ionicon"
								name="md-add"
								color={iOSColors.blue}
								size={18}
							/>
						) : null
					}
					clear
				/>
			)}
			{children}
		</View>
	);
};
Header.defaultProps = {
	divider: false,
	title: '',
	buttonTitle: '',
	icon: false,
};

const styles = StyleSheet.create({
	container: {
		padding: 16,
		borderBottomColor: iOSColors.lightGray,
		flexDirection: 'row',
		alignItems: 'center',
	},
	title: {
		flex: 1,
	},
});
export default Header;
