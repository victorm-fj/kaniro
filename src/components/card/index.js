// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes, Platform } from 'react-native';
import { iOSColors } from 'react-native-typography';

import Header from './Header';
import Content from './Content';
import Footer from './Footer';

type Props = {
	style: ViewPropTypes.style,
	children: React.Node,
};
const Card = (props: Props) => {
	return <View style={[styles.container, props.style]}>{props.children}</View>;
};
Card.Header = Header;
Card.Content = Content;
Card.Footer = Footer;

const styles = StyleSheet.create({
	container: {
		width: '90%',
		marginTop: 16,
		backgroundColor: iOSColors.white,
		borderRadius: 8,
		...Platform.select({
			ios: {
				shadowColor: 'rgba(0,0,0,0.1)',
				shadowOpacity: 1,
				shadowRadius: 5, //StyleSheet.hairlineWidth,
				shadowOffset: {
					width: 0,
					height: 2, //StyleSheet.hairlineWidth,
				},
			},
			android: {
				elevation: 6,
			},
		}),
	},
});
export default Card;
