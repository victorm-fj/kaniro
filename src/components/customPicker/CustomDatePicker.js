// @flow
import React from 'react';
import { StyleSheet, View, DatePickerIOS } from 'react-native';
import { Button } from 'react-native-elements';
import { iOSColors } from 'react-native-typography';

type Props = {
	setSelectedDate: (selectedDate: Date) => void,
	mode: 'date' | 'time' | 'datetime',
};
type State = {
	selectedDate: Date,
};
class CustomDatePicker extends React.Component<Props, State> {
	static defaultProps = { mode: 'datetime' };
	state = { selectedDate: new Date() };
	onDateChangeHandler = (selectedDate: Date) => {
		this.setState({ selectedDate });
	};
	onPressDone = () => {
		this.props.setSelectedDate(this.state.selectedDate);
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.overlay} />
				<View style={styles.pickerContainer}>
					<DatePickerIOS
						date={this.state.selectedDate}
						onDateChange={this.onDateChangeHandler}
						mode={this.props.mode}
					/>
					<Button
						title="Done"
						clear
						conatinerStyle={styles.buttonContainer}
						titleStyle={styles.buttonTitle}
						onPress={this.onPressDone}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		zIndex: 1000,
	},
	overlay: {
		flex: 1.7,
		backgroundColor: 'rgba(0,0,0,0.3)',
	},
	pickerContainer: {
		flex: 1.3,
		backgroundColor: iOSColors.white,
	},
	buttonContainer: {
		padding: 20,
		borderTopWidth: 1,
		borderColor: iOSColors.gray,
	},
	buttonTitle: {
		color: iOSColors.blue,
	},
});
export default CustomDatePicker;
