// @flow
import React from 'react';
import { StyleSheet, View, Picker } from 'react-native';
import { Button } from 'react-native-elements';
import { iOSColors } from 'react-native-typography';

type Option = { label: string, value: string };
type Props = {
	options: Array<Option>,
	setSelectedValue: (selectedValue: string, selectedIndex: number) => void,
};
type State = {
	selectedValue: string,
	selectedIndex: number,
};
class CustomPicker extends React.Component<Props, State> {
	state = { selectedValue: this.props.options[0].value, selectedIndex: 0 };
	onValueChangeHandler = (selectedValue: string, selectedIndex: number) => {
		this.setState({ selectedValue, selectedIndex });
	};
	onPressDone = () => {
		const { selectedValue, selectedIndex } = this.state;
		this.props.setSelectedValue(selectedValue, selectedIndex);
	};
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.overlay} />
				<View style={styles.pickerContainer}>
					<Picker
						selectedValue={this.state.selectedValue}
						onValueChange={this.onValueChangeHandler}
					>
						{this.props.options.map((option) => (
							<Picker.Item
								key={option.label}
								label={option.label}
								value={option.value}
							/>
						))}
					</Picker>
					<Button
						title="Done"
						clear
						conatinerStyle={styles.buttonContainer}
						titleStyle={styles.buttonTitle}
						onPress={this.onPressDone}
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		zIndex: 1000,
	},
	overlay: {
		flex: 1.7,
		backgroundColor: 'rgba(0,0,0,0.3)',
	},
	pickerContainer: {
		flex: 1.3,
		backgroundColor: iOSColors.white,
	},
	buttonContainer: {
		padding: 20,
		borderTopWidth: 1,
		borderColor: iOSColors.gray,
	},
	buttonTitle: {
		color: iOSColors.blue,
	},
});
export default CustomPicker;
