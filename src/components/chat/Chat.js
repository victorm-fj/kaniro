import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { GiftedChat, Bubble, SystemMessage } from 'react-native-gifted-chat';
// import messages from './messages';
// import oldMessages from './oldMessages';

class Chat extends React.Component {
	isMounted = false;
	isAlright = null;
	state = {
		messages: this.props.messages,
		// loadEarlier: true,
		// typingText: null,
		// isLoadingEarlier: false,
	};
	// componentDidMount() {
	// 	this.isMounted = true;
	// 	this.setState({ messages });
	// }
	// componentWillUnmount() {
	// 	this.isMounted = false;
	// }
	// onLoadEralier = () => {
	// 	this.setState({ isLoadingEarlier: true });
	// 	setTimeout(() => {
	// 		if (this.isMounted) {
	// 			this.setState((prevState) => ({
	// 				messages: GiftedChat.prepend(prevState.messages, oldMessages),
	// 				loadEarlier: false,
	// 				isLoadingEarlier: false,
	// 			}));
	// 		}
	// 	}, 1000); // simulate network
	// };
	onSend = (messages = []) => {
		this.setState((prevState) => ({
			messages: GiftedChat.append(prevState.messages, messages),
		}));
		// for demo purposes
		this.answerDemo(messages);
	};
	// answerDemo(messages) {
	// 	if (messages.length > 0) {
	// 		if (messages[0].image || messages[0].location || !this.isAlright) {
	// 			this.setState({ typingText: 'React Native is typing' });
	// 		}
	// 	}
	// 	setTimeout(() => {
	// 		if (this.isMounted) {
	// 			if (messages.length > 0) {
	// 				if (messages[0].image) {
	// 					this.onReceive('Nice picture!');
	// 				} else if (messages[0].location) {
	// 					this.onReceive('My favorite place');
	// 				} else {
	// 					if (!this.isAlright) {
	// 						this.isAlright = true;
	// 						this.onReceive('Alright');
	// 					}
	// 				}
	// 			}
	// 		}
	// 		this.setState({ typingText: null });
	// 	}, 1000);
	// }
	// onReceive(text) {
	// 	this.setState((previousState) => {
	// 		return {
	// 			messages: GiftedChat.append(previousState.messages, {
	// 				_id: Math.round(Math.random() * 1000000),
	// 				text: text,
	// 				createdAt: new Date(),
	// 				user: {
	// 					_id: 2,
	// 					name: 'React Native',
	// 					// avatar: 'https://facebook.github.io/react/img/logo_og.png',
	// 				},
	// 			}),
	// 		};
	// 	});
	// }
	// renderCustomActions(props) {
	// 	if (Platform.OS === 'ios') {
	// 		return <CustomActions {...props} />;
	// 	}
	// 	const options = {
	// 		'Action 1': (props) => {
	// 			alert('option 1');
	// 		},
	// 		'Action 2': (props) => {
	// 			alert('option 2');
	// 		},
	// 		Cancel: () => {},
	// 	};
	// 	return <Actions {...props} options={options} />;
	// }
	renderBubble = (props) => {
		return (
			<Bubble
				{...props}
				wrapperStyle={{
					left: {
						backgroundColor: 'rgb(170,170,170)',
					},
				}}
			/>
		);
	};
	renderSystemMessage = (props) => {
		return (
			<SystemMessage
				{...props}
				containerStyle={{
					marginBottom: 15,
				}}
				textStyle={{
					fontSize: 14,
				}}
			/>
		);
	};
	// renderCustomView(props) {
	// 	return <CustomView {...props} />;
	// }
	renderFooter = () => {
		if (this.state.typingText) {
			return (
				<View style={styles.footerContainer}>
					<Text style={styles.footerText}>{this.state.typingText}</Text>
				</View>
			);
		}
		return null;
	};

	render() {
		return (
			<GiftedChat
				messages={this.state.messages}
				onSend={this.onSend}
				// loadEarlier={this.state.loadEarlier}
				// onLoadEarlier={this.onLoadEarlier}
				// isLoadingEarlier={this.state.isLoadingEarlier}
				user={{
					_id: 1, // sent messages should have same user._id
				}}
				// renderActions={this.renderCustomActions}
				renderBubble={this.renderBubble}
				renderSystemMessage={this.renderSystemMessage}
				// renderCustomView={this.renderCustomView}
				// renderFooter={this.renderFooter}
				// keyboardShouldPersistTaps="never"
				renderAvatar={null}
			/>
		);
	}
}

const styles = StyleSheet.create({
	footerContainer: {
		marginTop: 5,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 10,
	},
	footerText: {
		fontSize: 14,
		color: '#aaa',
	},
});
export default Chat;
