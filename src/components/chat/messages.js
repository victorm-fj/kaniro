export default [
	{
		_id: Math.round(Math.random() * 1000000),
		text: 'Yes, and I use Gifted Chat!',
		createdAt: new Date(),
		user: {
			_id: 1,
			name: 'Developer',
		},
		sent: true,
		received: true,
		// location: {
		//   latitude: 48.864601,
		//   longitude: 2.398704
		// },
	},
	{
		_id: Math.round(Math.random() * 1000000),
		text: 'Are you building a chat app?',
		createdAt: new Date(),
		user: {
			_id: 2,
			name: 'React Native',
		},
	},
	{
		_id: Math.round(Math.random() * 1000000),
		text: 'Start chatting with our expert.',
		createdAt: new Date(),
		system: true,
	},
];
