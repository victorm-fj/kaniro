import React from 'react';
import { View, Text } from 'react-native';

const Error = ({ error }) => (
	<View
		style={{
			flex: 1,
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
		}}
	>
		<Text style={{ padding: 20, fontSize: 16, fontWeight: 'bold' }}>
			{error}
		</Text>
	</View>
);

export default Error;
