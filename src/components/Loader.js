// @flow
import React from 'react';
import { View } from 'react-native';
import { Bubbles } from 'react-native-loader';

const Loader = ({
	color,
	size,
	backgroundColor,
}: {
	color: string,
	size: number,
	backgroundColor: string,
}) => (
	<View
		style={{
			flex: 1,
			display: 'flex',
			justifyContent: 'center',
			alignItems: 'center',
			backgroundColor,
			position: 'absolute',
			top: 0,
			right: 0,
			bottom: 0,
			left: 0,
			zIndex: 10000,
		}}
	>
		<Bubbles size={size} color={color} />
	</View>
);

Loader.defaultProps = {
	color: '#1F2341',
	size: 12,
	backgroundColor: 'rgba(0,0,0,0.25)',
};
export default Loader;
