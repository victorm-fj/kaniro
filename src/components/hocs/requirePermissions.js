import React from 'react';
import { Alert, Platform } from 'react-native';
import Permissions from 'react-native-permissions';
import Loader from '../Loader';
import { asyncForEach } from './helpers';

export default (WrappedComponent, permissions) => {
	class RequirePermissions extends React.Component {
		state = {
			loading: true,
			permissions: permissions.reduce(
				(acc, value) => ({
					...acc,
					[value]: 'undetermined',
				}),
				{}
			),
		};
		async componentDidMount() {
			try {
				let response;
				if (permissions.length > 1) {
					response = await Permissions.checkMultiple(permissions);
					this.setState({ permissions: response /*, loading: false*/ });
				} else {
					const permission = permissions[0];
					response = await Permissions.check(permission);
					this.setState({
						permissions: { [permission]: response },
						/*loading: false,*/
					});
				}
			} catch (error) {
				console.log('checkPermissions error -', error);
				Alert.alert('Error', 'Something went wrong, please try again', [
					{
						text: 'Ok',
						onPress: () => {},
					},
				]);
			} finally {
				const permissionStatus = this.state.permissions[permissions[0]];
				if (permissionStatus === 'authorized') {
					this.setState({ loading: false });
				} else {
					this.askForPermissions();
				}
			}
		}
		askForPermissions = () => {
			const buttons = [];
			const cancelButton = {
				text: 'No thanks',
				onPress: () => console.log('askForPermissions denied'),
				style: 'cancel',
			};
			buttons.push(cancelButton);
			let requestPermissionsButton = null;
			let permissionsFlag = false;
			for (const permission of permissions) {
				if (this.state.permissions[permission] === 'denied') {
					permissionsFlag = true;
					return;
				}
			}
			if (Platform.OS === 'ios' && permissionsFlag) {
				requestPermissionsButton = {
					text: 'Open settings',
					onPress: this.openSettings,
				};
			} else {
				requestPermissionsButton = {
					text: 'Ok',
					// onPress: () =>
					// 	permissions.forEach((permission) =>
					// 		this.requestPermission(permission)
					// 	),
					onPress: this.requestPermissions,
				};
			}
			buttons.push(requestPermissionsButton);
			Alert.alert(
				'Permissions request',
				'I need you to grant some permissions before continuing',
				buttons
			);
		};
		requestPermissions = async () => {
			await asyncForEach(
				permissions,
				async (permission) => await this.requestPermission(permission)
			);
			this.setState({ loading: false });
		};
		openSettings = async () => {
			try {
				await Permissions.openSettings();
				alert('Back to app!');
			} catch (error) {
				console.log('openSettings', error);
			}
		};
		async requestPermission(permission) {
			const response = await Permissions.request(permission);
			this.setState((prevState) => ({
				permissions: { ...prevState.permissions, [permission]: response },
			}));
		}
		render() {
			if (this.state.loading) {
				return <Loader />;
			}
			return (
				<WrappedComponent
					{...this.props}
					requestPermissions={this.askForPermissions}
					permissionsStatus={this.state.permissions}
				/>
			);
		}
	}
	return RequirePermissions;
};
