// @flow
import React from 'react';
import { View, ViewPropTypes as RNViewPropTypes } from 'react-native';
import { Icon } from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import { iOSColors } from 'react-native-typography';

const ViewPropTypes = RNViewPropTypes || View.propTypes;
type ImageType = { type: string, data: string };
type Props = {
	addImage: (image: ImageType) => void,
	style?: ViewPropTypes.style,
	size: number,
};
class CustomImagePicker extends React.Component<Props> {
	static defaultProps = { style: {}, size: 36 };
	onPressSelectPhoto = () => {
		const options = {
			quality: 0.5,
			maxWidth: 500,
			maxHeight: 500,
			storageOptions: {
				skipBackup: true,
			},
		};
		ImagePicker.showImagePicker(options, (response) => {
			console.log('imagePicker response -', response);
			if (response.didCancel) {
				console.log('User cancelled photo picker');
			} else if (response.error) {
				console.log('imagePicker error -', response.error);
			} else {
				const { uri, data } = response;
				const image = {
					type: uri.split('.')[1].toLowerCase(),
					data,
				};
				this.props.addImage(image);
			}
		});
	};
	render() {
		return (
			<Icon
				onPress={this.onPressSelectPhoto}
				name="ios-camera-outline"
				type="ionicon"
				color={iOSColors.purple}
				size={this.props.size}
				containerStyle={this.props.style}
			/>
		);
	}
}

export default CustomImagePicker;
