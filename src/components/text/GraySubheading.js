// @flow
import * as React from 'react';
import { Text } from 'react-native';
import { human, iOSColors, systemWeights } from 'react-native-typography';

type Props = {
	style: Object,
	children: React.Node,
};
const GraySubheading = (props: Props) => {
	return (
		<Text
			style={[
				human.subhead,
				{ color: iOSColors.gray, paddingVertical: 5 },
				systemWeights.semibold,
				props.style,
			]}
		>
			{props.children}
		</Text>
	);
};
GraySubheading.defaultProps = { style: {} };

export default GraySubheading;
