// @flow
import * as React from 'react';
import { Text } from 'react-native';
import { human } from 'react-native-typography';

type Props = {
	style: Object,
	children: React.Node,
};
const MainTitle = (props: Props) => {
	return (
		<Text style={[human.title2, { paddingVertical: 5 }, props.style]}>
			{props.children}
		</Text>
	);
};
MainTitle.defaultProps = { style: {} };

export default MainTitle;
