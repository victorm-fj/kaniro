export { default as GraySubheading } from './GraySubheading';
export { default as GrayFootnote } from './GrayFootnote';
export { default as MainTitle } from './MainTitle';
