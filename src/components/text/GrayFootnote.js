// @flow
import * as React from 'react';
import { Text } from 'react-native';
import { human, iOSColors } from 'react-native-typography';

type Props = {
	style: Object,
	children: React.Node,
};
const GrayFootnote = (props: Props) => {
	return (
		<Text
			style={[
				human.footnote,
				{ color: iOSColors.gray, paddingVertical: 5 },
				props.style,
			]}
		>
			{props.children}
		</Text>
	);
};
GrayFootnote.defaultProps = { style: {} };

export default GrayFootnote;
