// @flow
import * as React from 'react';
import {
	StyleSheet,
	View,
	Animated,
	TouchableOpacity,
	Text,
} from 'react-native';
import Interactable from 'react-native-interactable';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { human } from 'react-native-typography';
import iOSColors from 'react-native-typography/dist/helpers/iOSColors';

type Props = {
	damping: number,
	tension: number,
	leftButtons: Array<Object>,
	rightButtons: Array<Object>,
	buttonWidth: number,
	height: number,
	children: React.Node,
};
class SwipeOut extends React.Component<Props> {
	static defaultProps = {
		damping: 1 - 0.7,
		tension: 300,
		rightButtons: [],
		buttonWidth: 75,
		leftButtons: [],
		height: 'auto',
	};
	deltaX = new Animated.Value(0);
	render() {
		const {
			damping,
			tension,
			leftButtons,
			rightButtons,
			buttonWidth,
			children,
		} = this.props;
		const onPress = () => {};
		const numberLeftButtons = leftButtons.length;
		const numberRightButtons = rightButtons.length;
		let snapPoints = [
			{
				x: 0,
				damping: 1 - damping,
				tension: tension,
			},
		];
		if (numberLeftButtons > 0) {
			snapPoints = [
				{
					x: numberLeftButtons * buttonWidth,
					damping: 1 - damping,
					tension: tension,
				},
				...snapPoints,
			];
		}
		if (numberRightButtons > 0) {
			snapPoints = [
				...snapPoints,
				{
					x: -(numberRightButtons * buttonWidth),
					damping: 1 - damping,
					tension: tension,
				},
			];
		}
		return (
			<View style={styles.container}>
				{leftButtons && (
					<View style={styles.buttonsContainer} pointerEvents="box-none">
						{leftButtons.map((button, index) => (
							<Animated.View
								key={`${button.title}${index}`}
								style={[
									styles.leftButtonContainer,
									{
										width: buttonWidth,
										backgroundColor: button.backgroundColor,
										transform: [
											{
												translateX: this.deltaX.interpolate({
													inputRange: [
														0,
														numberLeftButtons * buttonWidth +
															numberLeftButtons * numberLeftButtons +
															1,
													],
													outputRange: [
														-(index * buttonWidth),
														index * buttonWidth,
													],
												}),
											},
										],
									},
								]}
							>
								<TouchableOpacity
									onPress={button.onPress || onPress}
									style={styles.leftButton}
								>
									<Ionicons
										name={button.iconName}
										size={button.iconSize}
										color={button.titleColor}
									/>
									{button.title && (
										<Text
											style={[human.footnote, { color: button.titleColor }]}
										>
											{button.title}
										</Text>
									)}
								</TouchableOpacity>
							</Animated.View>
						))}
					</View>
				)}
				{rightButtons && (
					<View style={styles.buttonsContainer} pointerEvents="box-none">
						{rightButtons.map((button, index) => (
							<Animated.View
								key={`${button.title}${index}`}
								style={[
									styles.rightButtonContainer,
									{
										width: buttonWidth,
										backgroundColor: button.backgroundColor,
										transform: [
											{
												translateX: this.deltaX.interpolate({
													inputRange: [
														-(
															numberRightButtons * buttonWidth +
															numberRightButtons * numberRightButtons +
															1
														),
														0,
													],
													outputRange: [-(index * buttonWidth), buttonWidth],
												}),
											},
										],
									},
								]}
							>
								<TouchableOpacity
									onPress={button.onPress || onPress}
									style={styles.rightButton}
								>
									<Ionicons
										name={button.iconName}
										size={button.iconSize}
										color={button.titleColor}
									/>
									{button.title && (
										<Text
											style={[human.footnote, { color: button.titleColor }]}
										>
											{button.title}
										</Text>
									)}
								</TouchableOpacity>
							</Animated.View>
						))}
					</View>
				)}
				<Interactable.View
					horizontalOnly={true}
					snapPoints={snapPoints}
					animatedValueX={this.deltaX}
					boundaries={{
						left: -(numberRightButtons * buttonWidth),
						right: numberLeftButtons * buttonWidth,
					}}
				>
					<View style={styles.contentContainer}>{children}</View>
				</Interactable.View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: iOSColors.white,
		overflow: 'hidden',
	},
	buttonsContainer: {
		position: 'absolute',
		left: 0,
		right: 0,
		height: '100%',
	},
	leftButtonContainer: {
		height: '100%',
		position: 'absolute',
		top: 0,
		left: 0,
		justifyContent: 'center',
		alignItems: 'center',
	},
	leftButton: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	rightButtonContainer: {
		height: '100%',
		position: 'absolute',
		top: 0,
		right: 0,
		justifyContent: 'center',
		alignItems: 'center',
	},
	rightButton: {
		justifyContent: 'center',
		alignItems: 'center',
	},
	contentContainer: {
		left: 0,
		right: 0,
		backgroundColor: iOSColors.white,
	},
});
export default SwipeOut;
