import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { iOSColors } from 'react-native-typography';

import SwipeOut from './SwipeOut';

const rightButtons = [
	{
		title: 'Delete',
		titleColor: iOSColors.white,
		iconName: 'ios-trash',
		iconSize: 30,
		backgroundColor: iOSColors.blue,
	},
	{
		title: 'Active',
		titleColor: iOSColors.white,
		iconName: 'ios-checkmark-circle',
		iconSize: 30,
		backgroundColor: iOSColors.green,
	},
];

class Example extends React.Component {
	render() {
		return (
			<View style={styles.container}>
				<SwipeOut rightButtons={rightButtons}>
					<View style={styles.rowContent}>
						<View style={styles.rowIcon} />
						<View>
							<Text style={styles.rowTitle}>Row Title</Text>
							<Text style={styles.rowSubtitle}>
								Drag the row left and right
							</Text>
						</View>
					</View>
				</SwipeOut>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'white',
	},
	rowContent: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderColor: '#eeeeee',
	},
	rowIcon: {
		width: 50,
		height: 50,
		borderRadius: 25,
		backgroundColor: '#73d4e3',
		margin: 20,
	},
	rowTitle: {
		fontWeight: 'bold',
		fontSize: 20,
	},
	rowSubtitle: {
		fontSize: 18,
		color: 'gray',
	},
});
export default Example;
