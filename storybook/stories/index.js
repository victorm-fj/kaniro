import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { human, iOSColors, systemWeights } from 'react-native-typography';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Icon } from 'react-native-elements';
// import CircularSlider from 'react-native-circular-slider';

import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Button from './Button';
import CenterView from './CenterView';
import Welcome from './Welcome';
import Card from './Card';

storiesOf('Welcome', module).add('to Storybook', () => (
	<Welcome showApp={linkTo('Button')} />
));

storiesOf('Button', module)
	.addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
	.add('with text', () => (
		<Button onPress={action('clicked-text')}>
			<Text>Hello Button</Text>
		</Button>
	))
	.add('with some emoji', () => (
		<Button onPress={action('clicked-emoji')}>
			<Text>😀 😎 👍 💯</Text>
		</Button>
	));

storiesOf('Card', module)
	.addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
	.add('dummy example', () => (
		<Card>
			<Card.Header divider>
				<Text>Header</Text>
			</Card.Header>
			<Card.Content>
				<Text>Content</Text>
			</Card.Content>
			<Card.Footer divider>
				<Text>Footer</Text>
			</Card.Footer>
		</Card>
	))
	.add('your goal', () => (
		<Card>
			<Card.Header style={{ paddingBottom: 0 }}>
				<Text
					style={[
						human.footnote,
						{ color: iOSColors.gray },
						systemWeights.bold,
					]}
				>
					Your goal
				</Text>
			</Card.Header>
			<Card.Content
				style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 5 }}
			>
				<TouchableOpacity
					onPress={() => {}}
					style={{ flexDirection: 'row', alignItems: 'center' }}
				>
					<View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
						<Ionicons
							name="ios-locate-outline"
							size={40}
							color={iOSColors.blue}
						/>
						<Text style={[human.title2, { marginLeft: 10 }]}>
							What's your goal?
						</Text>
					</View>
					<Ionicons
						name="ios-arrow-forward"
						size={30}
						color={iOSColors.black}
					/>
				</TouchableOpacity>
			</Card.Content>
		</Card>
	))
	.add('points widget', () => (
		<Card>
			<Card.Header>
				<Icon
					type="ionicon"
					name="ios-arrow-dropleft"
					size={35}
					color={iOSColors.gray}
					onPress={() => {}}
				/>
				<View style={{ flex: 1 }}>
					<Text style={[human.body, { textAlign: 'center' }]}>TODAY</Text>
				</View>
				<Icon
					type="ionicon"
					name="ios-arrow-dropright"
					size={35}
					color={iOSColors.gray}
					onPress={() => {}}
				/>
			</Card.Header>
			<Card.Content style={{ flexDirection: 'row', alignItems: 'center' }}>
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Text style={[human.title2, systemWeights.semibold]}>2500</Text>
					<Text style={[human.footnote, { color: iOSColors.gray }]}>
						Potential
					</Text>
				</View>
				{/* <CircularSlider
				// startAngle
				// angleLength
				// onUpdate
				// segments
				// strokeWidth
				// radius
				// gradientColorFrom
				// gradientColorTo
				// showClockFace
				// bgCircleColor
				// stopIcon
				// startIcon
				/> */}
				<View
					style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
				>
					<Text style={[human.title2, systemWeights.semibold]}>2400</Text>
					<Text style={[human.footnote, { color: iOSColors.gray }]}>
						Remaining
					</Text>
				</View>
			</Card.Content>
		</Card>
	));
