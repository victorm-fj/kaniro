// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { iOSColors } from 'react-native-typography';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
};
const Header = (props: Props) => {
	return (
		<View
			style={[
				styles.container,
				{ borderBottomWidth: props.divider ? 1 : 0 },
				props.style,
			]}
		>
			{props.children}
		</View>
	);
};
Header.defaultProps = {
	divider: false,
};

const styles = StyleSheet.create({
	container: {
		padding: 15,
		borderBottomColor: iOSColors.lightGray,
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default Header;
