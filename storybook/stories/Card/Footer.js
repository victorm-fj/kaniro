// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { iOSColors } from 'react-native-typography';

type Props = {
	divider: boolean,
	style: ViewPropTypes.style,
	children: React.Node,
};
const Footer = (props: Props) => {
	return (
		<View
			style={[
				styles.container,
				{ borderTopWidth: props.divider ? 1 : 0 },
				props.style,
			]}
		>
			{props.children}
		</View>
	);
};
Footer.defaultProps = {
	divider: false,
};

const styles = StyleSheet.create({
	container: {
		padding: 15,
		borderTopColor: iOSColors.lightGray,
		flexDirection: 'row',
		alignItems: 'center',
	},
});
export default Footer;
