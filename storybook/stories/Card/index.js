// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';
import { iOSColors } from 'react-native-typography';

import Header from './Header';
import Content from './Content';
import Footer from './Footer';

type Props = {
	style: ViewPropTypes.style,
	children: React.Node,
};
const Card = (props: Props) => {
	return <View style={[styles.container, props.style]}>{props.children}</View>;
};
Card.Header = Header;
Card.Content = Content;
Card.Footer = Footer;

const styles = StyleSheet.create({
	container: {
		width: '90%',
		backgroundColor: iOSColors.white,
		borderRadius: 10,
		shadowColor: 'black',
		shadowOpacity: 0.1,
		shadowRadius: 5, //StyleSheet.hairlineWidth,
		shadowOffset: {
			height: 2, //StyleSheet.hairlineWidth,
		},
		elevation: 4,
	},
});
export default Card;
