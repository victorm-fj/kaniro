// @flow
import * as React from 'react';
import { StyleSheet, View, ViewPropTypes } from 'react-native';

type Props = {
	style: ViewPropTypes.style,
	children: React.Node,
};
const Content = (props: Props) => {
	return <View style={[styles.container, props.style]}>{props.children}</View>;
};

const styles = StyleSheet.create({
	container: {
		padding: 15,
	},
});
export default Content;
