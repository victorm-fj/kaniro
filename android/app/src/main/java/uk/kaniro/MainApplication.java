package uk.kaniro;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.wix.interactable.Interactable;
import com.reactlibrary.RNTooltipsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import com.horcrux.svg.SvgPackage;
import com.microsoft.codepush.react.CodePush;
import com.bugsnag.BugsnagReactNative;
import io.branch.rnbranch.RNBranchPackage;
import io.branch.referral.Branch;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.marianhello.bgloc.react.BackgroundGeolocationPackage;
import com.amazonaws.RNAWSCognitoPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.airbnb.android.react.maps.MapsPackage;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;
import com.ianlin.RNCarrierInfo.RNCarrierInfoPackage;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.avishayil.rnrestart.ReactNativeRestartPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(new MainReactPackage(),
            new Interactable(),
            new RNTooltipsPackage(),
            new SplashScreenReactPackage(),
            new SvgPackage(),
            new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG),
            BugsnagReactNative.getPackage(), new RNCarrierInfoPackage(), new ReactNativeContacts(),
          new RNBranchPackage(), new ReactNativeRestartPackage(), new BackgroundGeolocationPackage(),
          new RNAWSCognitoPackage(), new ImagePickerPackage(), new VectorIconsPackage(), new MapsPackage(),
          new RNFusedLocationPackage());
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    BugsnagReactNative.start(this);
    Branch.getAutoInstance(this);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
